import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanMatch, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { DataService } from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanMatch {
  // isLoggedUser:any
  currentUserData:any;
  constructor(private _router:Router, private _dataService:DataService ) {
  
  }
  canMatch(route: Route, segments: UrlSegment[]): Observable<boolean>|Promise<boolean>|boolean {
    this.currentUserData = this._dataService.GetStorage('currentUserData');
    if(this.currentUserData){
      return true

    }
    else {
     return this._router.navigate(['/auth/login']);
    }
  }
  
}
