export interface Interfaces {}

export interface CardDetails {
  imgPath?: string;
  countValue: number | string;
  countHeadingText: string;
  featherIconNames?: string;
  backgroundColors: Array<string>;
}

export interface TestData {
  ReferenceId: number;
  ReferenceKey: string;
  DisplayName: string;
  ContactNumber: any;
  EmailAddress: string;
  Address: string;
  MerchantId: any;
  MerchantKey: string;
  CreateDate: string;
  StatusId: any;
  StatusCode: string;
  StatusName: string;
  Categories: {
    CategoryId: number;
    CategoryKey: string;
    CategoryName: string;
  }[];
}

export interface NotificationData {
  headerText: string;
  contentText: string;
  buttonText: string;
}

export interface CashierData {
  cashierCode: string;
  firstName: string;
  middleName: string;
  lastName: string;
  gender: string;
  phoneNumber: string;
  emailAddress: string;
  isDeleted: boolean;
}

export interface CashierCount {
  responseCode: number;
  responseMessage: string;
  data: {
    totalActive: number;
    totalInActive: number;
    totalIsBlocked: number;
  };
}

export interface CashierDetailsResponse {
  responseCode: number;
  responseMessage: string;
  data: CashierSingleResponse;
}

export interface CashierSingleResponse {
  cashierCode: string;
  firstName: string;
  middleName: string;
  lastName: string;
  gender: string;
  phoneNumber: string;
  emailAddress: string;
  userId: string;
  isDeleted: boolean;
  id: number;
  createdBy: string;
  dateCreated: string;
  modifiedBy: string;
  dateModified: string;
  authUser: AuthUser | null;
}

export interface CashierListResponse {
  responseCode: number;
  responseMessage: string;
  data: CashierSingleResponse[];
}

export interface AuthUser {
  isActive: boolean;
  isPasswordChanged: false;
  dateCreated: string;
  lastModified: string;
  lastLogin: string;
  otp: string | number | null;
  forgotPasswordOTP: string | number | null;
  otpSubmittedTime: string;
  forgotPasswordOTPSubmitedTime: string;
  refreshToken: string | null;
  refreshTokenExpiryTime: string | null;
  id: string;
  userName: string;
  normalizedUserName: string;
  email: string;
  normalizedEmail: string;
  emailConfirmed: boolean;
  passwordHash: string;
  securityStamp: string;
  concurrencyStamp: string;
  phoneNumber: string | null;
  phoneNumberConfirmed: boolean;
  twoFactorEnabled: boolean;
  lockoutEnd: boolean;
  lockoutEnabled: boolean;
  accessFailedCount: number;
}

export interface SalesSummaryResponse {
  status: string;
  statusCode: number;
  message: string;
  data: SalesSummaryData;
}

export interface SalesSummaryData {
  totalInvoices: number;
  totalSalesAmount: number;
  salesSummary: SalesSummary[];
}

export interface SalesSummary {
  invoicesId: number;
  date: string;
  customer: string;
  customerPhoneNumber: string;
  salesAmount: number;
  store: string;
  storeAddress: string | null;
  cashier: string | null;
  cashierID: number;
  pos: string | null;
  posOwner: string | null;
}

export interface ProductInventoryResponse {
  status: string;
  statusCode: number;
  message: string;
  data: ProductInventoryData;
}

export interface ProductInventoryData {
  totalProduct: number;
  totalStockAvailable: number;
  totalStockSold: number;
  totalOpeningStock: number;
  totalSaleAmount: number;
  productInventories: ProductInventory[];
}

export interface ProductInventory {
  itemName: string;
  category: string;
  stockInHand: number;
  totalStockSold: number;
  inventoryAssetStock: number;
  openingStock: number;
}

export interface StockUpdateResponse {
  status: string;
  statusCode: number;
  message: string;
  data: StockUpdate[];
}

export interface StockUpdate {
  productName: string;
  date: Date | null;
  reason: string;
  updatedBy: string | null;
  updation: number;
}

export interface ProductWiseResponse {
  status: string;
  statusCode: number;
  message: string;
  data: ProductWiseData;
}

export interface ProductWiseData {
  allProduct: number;
  totalQuantitySold: number;
  totalSalesWithoutTax: number;
  totalSalesWithTax: number;
  productWises: ProductWise[];
}

export interface ProductWise {
  itemName: string;
  category: string;
  quantitySold: number;
  sales: number;
  salesWithTax: number;
}

export interface ReturnSaleResponse {
  status: string;
  statusCode: number;
  message: string;
  data: ReturnSaleData;
}

export interface ReturnSaleData {
  totalSalesReturn: number;
  totalReturnAmount: number;
  returnSale: ReturnSale[];
}

export interface ReturnSale {
  invoiceNo: string;
  returnDate: string;
  salesDate: string;
  customer: string | null;
  invoiceAmount: number;
  returnCreditAmount: number;
  store: string;
}

export interface CreateDiscountRequest {
  merchantId: number;
  name: string;
  posName: string;
  description: string;
  type: string;
  startDate: string;
  endDate: string;
  startTime: string;
  endTime: string;
  storeIds: number[];
}
export interface posSalesReturnRes {
  responseCode: number;
  responseMessage: string;
  data: CashierSingleResponse;
}

export interface CreditNoteResponse {
  statusCode: number;
  status: string;
  message: string;
  data: CreditNoteObject;
}

export interface CreditNoteObject {
  issuedCreditNotes: number;
  totalCreditedAmount: number;
  creditNotes: CreditNotes[];
}

export interface CreditNotes {
  creditNoteId: 12;
  creditNoteNumber: string;
  issueDate: string;
  invoiceNumber: string;
  customerMobile: number;
  customerName: string;
  creditedAmount: number;
  refundedCredit: number;
  availableCredit: number;
  status: string;
}

export interface DiscountResponse {
  responseCode: number;
  responseMessage: string;
  data: Discounts[];
}

export interface Discounts {
  discountName: string;
  discountType: string;
  description: string;
  storeLocations: any[];
  id?:number;
}
export interface CreditNoteDetails {
  statusCode: number;
  status: string;
  message: string;
  data: Details;
}

export interface Details {
  creditNoteId: number;
  creditNoteNumber: string;
  issueDate: string;
  invoiceNumber: string;
  customerMobile: number;
  customerName: string;
  creditedAmount: number;
  refundedCredit: number;
  availableCredit: number;
  status: string;
  returnedItems: ReturnedItems[];
}

export interface ReturnedItems {
  productId: number;
  productUrl: string;
  productName: string;
  unitPrice: string;
  quantity: number;
  totalPrice: number;
}

export interface CategoryResponse {
  responseCode: number;
  responseMessage: string;
  data: CategoryData[];
}

export interface CategoryData {
  name: string;
  imageurl: string | null;
  subCategories: any[];
  products: any[];
  policyId: number | null;
  policy: any;
  id: number;
  createdBy: string | null;
  dateCreated: string;
  modifiedBy: string | null;
  dateModified: string | null;
  categoryId:number | null;
}
//for category
export interface GetCategoriesRes {
  responseCode: number;
  responseMessage: string;
  data: CategoriesData[];
}

export interface CategoriesData {
  // name: string;
  // imageurl: string;
  // subCategories: string[];
  // products: string[];
  // policyId: number;
  // policy: string;
  // id: number;
  // createdBy: string;
  // dateCreated: string;
  // modifiedBy: string;
  // dateModified: string;
  name: string;
  subcategoriesName: string[];
  imageUrl: string;
  createdBy: string;
  createdDate: string;
  updateDate: string;
  totalProduct: number;
}

export interface GetSubCategoriesRes {
  responseCode: number;
  responseMessage: string;
  data: SubCategoriesData[];
}

export interface SubCategoriesData {
  name: string;
  products: string[];
  id: number;
  createdBy: string;
  dateCreated: string;
  modifiedBy: string;
  dateModified: string;
}

export interface StoreListResponse {
  responseCode: number;
  responseMessage: string;
  data: Store[];
}

export interface Store {
  storeId: number;
  merchantId: number;
  storeName: string;
  merchant: string;
  country: string;
  state: string;
  city: string;
  isTracked: boolean,
  address: string;
  dateCreated: string;
}

export interface BackendGeneralFormat<T =  null>{
  responseCode: number,
  responseMessage: boolean | string,
  data: T
}

export interface ATax {
  merchantId: number
  taxId: number
  taxName: string
  rate: number
  numberOfProducts: number
  locationName: string[]
}

export interface ATaxFromServer extends Pick<ATax, 'rate'> {
  taxName: any
  taxOnProducts: TaxOnProduct
  enableTax: false
  isDefault: false
  locationName: any[]
  merchantTaxes: any[]
  products: any[],
  categories: any[],
  productVariants: any[],
  id: number,
  createdBy:null,
  dateCreated: string,
  modifiedBy: string,
  dateModified: string
}

export interface AStoreLocation {
  id: number,
  city: string,
  state: string
}


export interface ATaxToBeCreated{
    merchantId: number,
    name: string,
    rate: number,
    taxOnProducts: string,
    enableTax: boolean,
    locationsName?: string[],
    storeLocationIds: number[]

}

export interface ReturnPolicyTobeCreated{
    merchantId: number,
    name: string,
    charges: number,
    termAndConditions: string,
    isDefault: boolean,
    productReturnDays: number,
    productIds: number[],
    categories: number[]

}

export interface CreatedPolicy{
    policyId: number,
    policyName: string,
    termsAndCondition: string,
    charges: number,
    returnDays: number
  }

  export interface ExtendedCreatedPolicy extends CreatedPolicy {
    merchantId: string, categories: any[], termAndConditions: string,products: any[],name: string, isDefault: boolean, productReturnDays: number
    createdBy: string, modifiedBy: string, id: any, dateModified: string}

  export interface TrackingDs{

      merchantId: number,
      storeIds: number[],
      status: boolean
  }

  export interface getStoresParams {
    MerchantId:  number,
    DateFrom?: string,
    DateTo?: string,
    SearchKey?: string,
    SortKey?: string
}

export interface TaxAndStockSettings{
  enableStockTracking: boolean,
  enableTaxOnProduct: boolean,
  taxInfo: TaxOnProduct
}

export interface EnableNotificationReqBody{
    enableNotificationForStockAlert: boolean,
    enableProductReturnAlert: boolean,
    enableEmailAlert: boolean,
    autoGenerateCreditNote: boolean
  }
export enum TaxOnProduct  {
  Include='Include',
  Exclude='Exclude'
}

export interface SearchParams{
  PageSize: number,
  PageNumber: number
}

export interface ACategoryFromStore {
  name: string
  categoryId: number
  imageurl: any
  subCategories: any[]
  products: any[]
  policyId?: number
  policy: any
  id: number
  createdBy: any
  dateCreated: string
  modifiedBy?: string
  dateModified?: string
}



  export interface Product {
    id: number
    name: string
    description: string
    unitPrice: number
    salesPrice: number
    discountPrice: number
    sku: string
    otherTermsAndCondition: string
    isDiscounted: boolean
    isLoose: boolean
    hasVariant: boolean
    unit: string
    barcodeType: string
    barcodeValue: string
    mpn: any
    brandName: string
    warranty: string
    rewardPercentage: number
    height: any
    length: any
    breadth: any
    variants: Variant[]
  }

  export interface Variant {
    id: number
    variantName: string
    sku: string
    unitPrice: number
    salesPrice: number
    discountPrice: number
    isDiscounted: boolean
    isDefaultVariant: boolean
    stock: Stock
    parentProductId?: number
  }

  export interface Stock {
    id: number
    availableUnits: number
    isLowStockAlertEnabled: boolean
    lowStockAlertValue: number
    productionDate: string
    expiryDate: string
  }
export interface SalesGraphAnalytics {
  totalQuantitySold: number;
  periodType: string;
}
export interface SalesTrendData {
  salesAmount: number;
  salesWithTax: number;
  salesWithoutTax: number;
  revenue: number;
  salesGraphs: SalesGraphAnalytics[];
}
export interface SalesAnalyticsResponse {
  status: string;
  statusCode: number;
  message: string;
  data: SalesTrendData;
}


export interface ReturnDataAnalytics {
  status: string;
  statusCode: number;
  message: string;
  data: ReturnDataDetailsAnalytics;
}

export interface ReturnDataDetailsAnalytics {
  returnCount: number;
  returnAmountWithTax: number;
  returnAmountWithoutTax: number;
  returnGraph: ReturnGraphItem[];
}

export interface ReturnGraphItem {
  periodType: string;
  returnAmount: number;
}

export interface SalesInvoiceData {
  status: string;
  statusCode: number;
  message: string;
  data: SalesInvoiceDataPayload;
}
export interface SalesInvoiceDataPayload {
  cashPaymentWithCash: number;
  cashPaymentWithoutCash: number;
  cardPaymentWithCash: number;
  cardPaymentWithoutCash: number;
  totalDifference: number;
  may18InvoiceCount: number;
  may11InvoiceCount: number;
  invoiceGraph: InvoiceGraphItem[];
}
export interface InvoiceGraphItem {
  periodType: string;
  totalAMount: number;
}


export interface InvoiceListResponse{
  responseCode: number;
  responseMessage: string;
  data: Invoice[];
}

export interface Invoice {
  invoiceId: number;
  terminalId: number | null;
  cashierId: number;
  customerId: number;
  invoiceAmount: number;
  channelId: number;
  storeId: number;
  statusId: number;
  merchantId: number;
  id: number;
  createdBy: string;
  dateCreated: string;
  modifiedBy: string;
  dateModified: string;
}

export interface PosListResponse {
  responseCode: number;
  responseMessage: string;
  data: {
    allTerminals: number;
    activeTerminals: number;
    idleTerminals: number;
    deadTerminals: number;
    unusedTerminals: number;
    terminals: Terminal[];
  };
}

export interface Terminal {
  id: number;
  serialNumber: string;
  terminalID: string;
  lastTransactionDate: string;
  merchantId: number;
  merchantName: string;
  storeId: number;
  storeName: string;
  storeLocation: string;
  type: string;
  issuerBankId: number;
  issuerBank: string;
  issuerBankIconUrl: string;
  providerId: number;
  provider: string;
  dateCreated: string;
}

export interface GeneralOverviewListResponse {
  status: string;
  statusCode: number;
  message: string;
  data: {
    productSold: number;
    salesAmount: number;
    revenue: number;
    invoicePaid: number;
    subSalesAmount: number;
    subSalesWithTax: number;
    subSalesWithoutTax: number;
    subRevenue: number;
    productAndStockDetails: {
      totalProduct: number;
      available: number;
      lowStock: number;
      outOfStock: number;
      unAvailable: number;
    },
    overviewSalesGraghsData: [
      {
        amount: number;
        periodType: string
      }
    ]
  }
}

export interface TopProductResponse{
  status: string;
  statusCode: number;
  message: string;
  data: [
    {
      productName: string;
      amount: number;
    }
  ]
}

export interface TopCategoryResponse{
  status: string;
  statusCode: number;
  message: string;
  data: [
    {
      categoryName: string;
      amount: number;
    }
  ]
}

export interface ProductGraphData {
  status: string;
  statusCode: number;
  message: string;
  data: {
    soldStock: number;
    availableStock: number;
    assetValue: number;
    openingStock: number;
    productInventoryGraphs: ProductInventoryGraph[];
  };
}

export interface ProductInventoryGraph {
  assetValue: number;
  availableStock: number;
  periodType: string;
}


export interface StockUpdateData {
  status: string;
  statusCode: number;
  message: string;
  data: {
    lowStockAlertCount: number;
    salesWithTax: number;
    salesWithoutTax: number;
    revenue: number;
    juneTotalStockUpdate: number;
    augustTotalStockUpdate: number;
    tOtalDifference: number;
    stockUpdateGraphs: StockUpdateGraph[];
  };
}

export interface StockUpdateGraph {
  totalQuantitySold: number;
  periodType: string; 
}
