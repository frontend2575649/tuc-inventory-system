export class Responses {
  errors: SingleResponse[] = [];
  success: SingleResponse[] = [];
 }

export interface SingleResponse {
     status: number;
     IsSuccess: boolean;
     details: string;
 }

export interface Observer {
 next: Function;
 error: Function;
 complete: Function;
 }

export interface PaginatedResponse<T> {
 items: T[];
 total: number;
}
export interface PaginationValue {
 page: number;
 pageSize: number;
}
