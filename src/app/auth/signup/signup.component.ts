import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// import { HelperService } from 'src/app/utils/helper.service';
import * as Feather from "feather-icons";
import { NotificationService } from '../../services/notification.service';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  countryIcon = "../../../assets/img/Signup/nigeria_icon.png"
  Form_SignUp!: FormGroup;
  ShowPassword: boolean = true;
  SelectedCountry: any;
  selectedCountryobj: any;
  Countries = [];
  selectedCountryISD = "+234"
  selectedAccountId: any;
  email_lowercase: any;
  AccountId: any;
  countryIconArray: any = [
    {

      Name: "Nigeria",
      IconUrl: "../../../assets/img/countries/nigeria_icon.png",
      areaCode: "+234",
      Iso: "ng",
      CurrencyNotation: "NGN",
      id: 2,
    },
    {

      Name: "Ghana",
      IconUrl: "../../../../assets/img/countries/ghana_icon.png",
      areaCode: "+233",
      Iso: "gh",
      id: 3,
    },
    {
      Name: "Kenya",
      IconUrl: "../../../../assets/img/countries/kenya_icon.png",
      areaCode: "+254",
      Iso: "key",
      id: 1,
    }
  ];
  constructor(
    public _FormBuilder: FormBuilder,
    private router: Router,
    public _apiService: ApiService,
    public _dataService: DataService,
    public _notification: NotificationService

  ) {
   
  }
 
  ngOnInit(): void {
    this.Form_SignUp_Load();
  }
 
  Form_SignUp_Load() {
    this.Form_SignUp = this._FormBuilder.group({
      contactPersonName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256), Validators.pattern("^[a-zA-Z _-]*$")])],
      merchantName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(128)])],
      emailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2), Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$')])],
      phoneNumber: [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern("^[1-9]{1}[0-9]{9}$")])],
      countryId: [null],
      cityId: 1,
      stateId: 2,
      longitude: "6.4698",
      latitude: "3.5852",
      address:"string"
    })
  }
  changeCountry() {
    let Selected_country = this.Form_SignUp.controls['countryId'].value;
    console.log("Selected_country",Selected_country)
    this.selectedCountryobj = this.Countries.find((ele: any) => ele.id == Selected_country);
    this.selectedCountryISD = this.selectedCountryobj.areaCode;
    let selectedIconObj = this.countryIconArray.find((ele: any) => ele.areaCode == this.selectedCountryobj.areaCode);
    this.countryIcon = selectedIconObj.IconUrl;
  }



  conutrystatus:boolean=false;
  keniyaconutry:boolean=false;
  onChange(deviceValue:any) {
    this.SelectedCountry = deviceValue.target.value;
    console.log(this.SelectedCountry)
    if( this.SelectedCountry=='2')
    {
      this.conutrystatus=true;
      
    
    }
    else{
      this.conutrystatus=false;
    }

    // else{
    //   if(this.SelectedCountry=='2')
    //   {
    //     this.keniyaconutry=true;
    //   }
     
    // }
    console.log(this.conutrystatus)
   
}

gotootp()
{
  //this.router.navigate(['/otpverification/email']);
  this.router.navigate(['auth/otpverification/']);
}

Form_SignUpUser_Process(_FormValue: any) {
  if (_FormValue.emailAddress) {
    let str = _FormValue.emailAddress;
    this.email_lowercase = "";
    for (let chars of str) {
      let value = chars.charCodeAt();
      if (value >= 65 && value <= 90) {
        this.email_lowercase += String.fromCharCode(value + 32);
      } else {
        this.email_lowercase += chars;
      }
    }
  }
}

registration_Form_process(_FormValue: any) {
  if (_FormValue) {
    this._dataService.SaveStorage('_FormValue', _FormValue);
  }

  this.Form_SignUpUser_Process(_FormValue);
  let RequestBody = _FormValue;
  console.log("RequestBody",RequestBody)
  RequestBody.emailAddress = this.email_lowercase;
  RequestBody.countryId= this.SelectedCountry;
  // RequestBody.accountTypeId = parseInt(_FormValue.accountTypeId);
  if (this.AccountId) {
    this.selectedAccountId = this.AccountId;
  }
  else {
    this.selectedAccountId = _FormValue.accountTypeId;
  }
 // RequestBody.accountTypeId = parseInt(this.selectedAccountId);

  // RequestBody.phoneNumber=this.selectedCountryISD.concat(RequestBody.phoneNumber);
  this._apiService.postData( RequestBody,'onboarding/onboardMerchant').subscribe((Response: any) => {
    if (Response && Response.responseCode == "200") {
     
      setTimeout(() => {
        this._notification.NotifySuccess(Response.responseMessage);
      }, 100);
     
   
    this.setInterval();
   

    }
    else {
      // this._notification.NotifyError(Response.message);
      this._notification.NotifyError(Response.responseMessage);
    }
  },
    _Error => {
      this._notification.HandleException(_Error);
    });
}

intervalTime: any;
email:any;

setInterval() {
  this.email=this.email_lowercase
  this.intervalTime = setInterval(() => {
    this.router.navigate(['auth/otpverification/'+this.email]);
  }, 5000);
}
}
