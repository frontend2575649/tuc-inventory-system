import { Component, OnInit } from '@angular/core';

import { NotificationService } from '../../services/notification.service';
import * as Feather from "feather-icons";
import { DataService } from 'src/app/services/data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
@Component({
  selector: 'app-otpvarification',
  templateUrl: './otpverification.component.html',
  styleUrls: ['./otpverification.component.css']
})
export class OtpverificationComponent implements OnInit{
  businessemailToValidate: any;
  userEnteredOtp:any;
  inValidOtp: boolean = false;
  step: number = 1;
  ShowPassword: boolean = true;
  accShowPassword: boolean = true;
  accconfirmShowPassword: boolean = true;
  Form_createPassword!: FormGroup;
  ispwdContainsNum = true;
  ispwdContainsUC = true;
  ispwdContainsLC = true;
  IsminLength = true;
  isPwdContainsSC = true;
  passwordMissmatch: boolean = false;
  confirmShowPassword: boolean = true;
  userEnteredPassword: any;
  sterperValue:any;
constructor( 
  public _notification:NotificationService,
  public _dataService:DataService,
  public _FormBuilder:FormBuilder,
  private router: Router,
  public _apiService: ApiService,

  public activeRoute : ActivatedRoute
  ){
this.Form_Password_Load();
this.activeRoute.params.subscribe((params: Params) => {
  let email = params["email"];
  this.businessemailToValidate=email;
  console.log(email);
});
}
ngOnInit(): void {
  this.setSteper();
}

onOtpChange(event: any) {
  this.userEnteredOtp = event;
}

setSteper() {
  this.sterperValue = this._dataService.GetStorage('isSteper');
  if (this.sterperValue) {
    this.step = this.sterperValue;
  }
  else {
    this.step = 1;
  }
}
GotoNext() {
  Feather.replace();
  this.step = this.step + 1;
  this._dataService.SaveStorage('isSteper', this.step);
 
  if (this.step == 2) {
    this.ShowPassword = false;
    setTimeout(() => {
      this.ShowPassword = true;
    }, 100);
  }
}

verifyOtp() {
  if (this.userEnteredOtp) {
    this.inValidOtp = false;
    let RequestBody = {
      email: this.businessemailToValidate,
      otp: this.userEnteredOtp
    }
    this._apiService.getDatawithFilter( 'accounts/validateotp',RequestBody).subscribe((Response: any) => {
      if (Response && Response.responseCode == "200") {
        this._notification.NotifySuccess(Response.responseMessage);
        this.GotoNext();
      }
      else {
        this._notification.NotifyError(Response.responseMessage);
      }
    },
      _Error => {
        this._notification.HandleException(_Error);
      });
  }
  else {
    this.inValidOtp = true;
  }
}

ResendOtp() {
  
  let filterData = {
    email:  this.businessemailToValidate,
  }
  this._apiService.getDatawithFilter('accounts/resendotp', filterData).subscribe((Response:any) => {
    if (Response && Response.responseCode == "200") {
      this._notification.NotifySuccess(Response.responseMessage);
     
    }
  },
    _Error => {
      this._notification.HandleException(_Error);
    });
}

Form_Password_Load() {
  this.Form_createPassword = this._FormBuilder.group({
    Password: ['', Validators.required]
  })
}

createPassword_Form_process(_FormValue: any) {
  let RequestBody = {
    newPassword: _FormValue.Password,
    email: this.businessemailToValidate
  }
  this._apiService.postData(RequestBody, 'accounts/resetpassword').subscribe((Response: any) => {
    if (Response && Response.responseCode == "200") {
      this._notification.NotifySuccess(Response.responseMessage);
      this.userEnteredPassword = _FormValue.Password;
      this.router.navigate(['auth/login']);
      // let logInData = {
      //   password: this.userEnteredPassword,
      //   username: this.businessemailToValidate
      // }
      // this.login(logInData);
      this._dataService.DeleteStorage('isSteper');
     
      // this.GotoNext();
    }
    else {
      this._notification.NotifyError(Response.responseMessage);
    }
  },
    _Error => {
      this._notification.HandleException(_Error);
    });
}

login(_FormValue: any) {
  this._apiService.postData(_FormValue, 'accounts/login').subscribe(Response => {
    if (Response && Response.responseCode == "200") {
      this._dataService.SaveStorage('currentUserData', Response.data);
      this._apiService.getHttpOptions();
      this._dataService.setCurrentUserData();
      this._notification.NotifySuccess(Response.message);
       this.router.navigate(['/overview']);
     
     // this._helperService.AppConfig.logedInUserId = Response.data.id

    }
    else {
      this._notification.NotifyError(Response.message);
    }
  },
    _Error => {
      this._notification.HandleException(_Error);
    });
}

checkPassword(event: any) {
  let conPassword = event.target.value;
  let pass = this.Form_createPassword.controls['Password'].value;
  if (conPassword != pass) {
    this.passwordMissmatch = true;
  }
  else {
    this.passwordMissmatch = false;
  }

}
ToogleShowHideAccountPassword() {
  this.accShowPassword = !this.accShowPassword;
}

ToogleShowHideaccountconfirmPassword() {
  this.accconfirmShowPassword = !this.accconfirmShowPassword;
}
}
