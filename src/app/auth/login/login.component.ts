import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';
import { NotificationService } from '../../services/notification.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy {
  ShowPassword: boolean = true;
  _Form_Login_Processing = false;
  LoginForm!: FormGroup;

  ngOnDestroy(): void {}
  ngOnInit(): void {
    window.localStorage.clear();
  }
  constructor(
    private router: Router,
    public _apiService: ApiService,
    public _dataService: DataService,
    public _notification: NotificationService,
   //private route: ActivatedRoute,
    public _FormBuilder: FormBuilder,
  
  ) {
    this.LoginForm = _FormBuilder.group({
      username: [
        null,
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.minLength(2),
          Validators.pattern(
            '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$'
          ),
        ]),
      ],
      password: ['', Validators.required],
    });
  }

  login(_FormValue: any) {
    this._apiService.postData(this.LoginForm.value, 'accounts/login').subscribe(Response => {
      if (Response && Response.responseCode == "200") {
        console.log(Response);
        this._dataService.SaveStorage('currentUserData', Response.data);

          const user = this._dataService.GetStorage('currentUserData');
          this.router.navigate(['/overview']);
        
       
      }
      (_Error: any) => {
        this._notification.HandleException(_Error);
      }
  });
  }

  
  ToogleShowHidePassword(): void {
    this.ShowPassword = !this.ShowPassword;
  }
  
}

// const accessToken = resData.token.accessToken;
// const userToken = resData.token.userToken;
// const exp =
//   resData.token.expiresIn * 60000 + new Date().getTime();

//   console.log(exp, 'expiry');
// const refreshToken = resData.token.refreshToken;
// const data = { accessToken, userToken, exp, refreshToken };
// console.log(data);

// localStorage.setItem('AfreximVMS', JSON.stringify(data));
