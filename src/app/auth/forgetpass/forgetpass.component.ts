import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { NotificationService } from '../../services/notification.service';
import { ApiService } from '../../services/api.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-forgetpass',
  templateUrl: './forgetpass.component.html',
  styleUrls: ['./forgetpass.component.css']
})
export class ForgetpassComponent implements OnInit {
  ShowConfPassword:boolean=true;
  isPwdContainsSC = true;
  ispwdContainsUC = true;
  ispwdContainsLC = true;
  ispwdContainsNum = true;
  IsminLength = true;
  ShowPassword: boolean = true;
  Form_ResetPassword!: FormGroup;
  Form_BusinessEmail!: FormGroup;
  businessemailToValidate: any;
  filterData = {};
  constructor(
    public _FormBuilder: FormBuilder,
    private router: Router,
    public _apiService: ApiService,
    public _dataService: DataService,
    public _notification: NotificationService
    
  ) {
   
    this.Form_BusinessEmailLoad();
  }
  ngOnInit(): void {

  }
  Form_ResetFormLoad() {
    this.Form_ResetPassword = this._FormBuilder.group({
      Password: [null, Validators.required],
      confirmPassword: [null, Validators.required],
    })
  }

  Form_BusinessEmailLoad() {
    this.Form_BusinessEmail = this._FormBuilder.group({
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2), Validators.pattern('^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$')])]
    })
  }

  gotootp(email:any)
{
  //this.router.navigate(['/otpverification/email']);
  this.router.navigate(['auth/otpverification/'+email]);
}

Form_SendOtp_Process(_FormValue: any) {
  if (_FormValue) {
    this.businessemailToValidate = _FormValue.EmailAddress;
  }
  this.filterData = {
    email: _FormValue.EmailAddress,
  }
  this._apiService.getDatawithFilter('accounts/resendotp', this.filterData).subscribe((Response:any) => {
    if (Response && Response.responseCode == "200") {
      this._notification.NotifySuccess(Response.responseMessage);
      this.gotootp(this.businessemailToValidate);
    }
    else{
      this._notification.NotifyError(Response.responseMessage);
    }
  },
    _Error => {
      this._notification.HandleException(_Error);
    });
}
}