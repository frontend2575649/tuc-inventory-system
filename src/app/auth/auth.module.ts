import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from '../auth/auth.component';
import { NgOtpInputModule } from 'ng-otp-input';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgetpassComponent } from './forgetpass/forgetpass.component';
import { OtpverificationComponent } from './otpverification/otpverification.component';
//import { CreatepassComponent } from './createpass/createpass.component';
import { FeatherModule } from 'angular-feather';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
const routes: Routes = [
  {
      path: '',
      component: AuthComponent,
      children: [
          {
              path: 'login',
              component: LoginComponent
          },
          {
              path: 'signup',
              component: SignupComponent
          },
          {
              path: 'forgotpassword',
              component: ForgetpassComponent
          },
          // {
          //     path: 'createpass',
          //     component: CreatepassComponent
          // },
          {
              path: 'otpverification/:email',
              //path: 'otpverification',
              component: OtpverificationComponent
          },
      ]
  }
]
@NgModule({
  declarations: [LoginComponent,AuthComponent,LoginComponent,SignupComponent,OtpverificationComponent,ForgetpassComponent],
  imports: [
    CommonModule,FormsModule,ReactiveFormsModule,NgSelectModule,NgOtpInputModule,
    FeatherModule, 
    RouterModule.forChild(routes), 
  ]
})
export class AuthModule { }
