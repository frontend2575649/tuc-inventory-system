import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ProductWiseResponse, ReturnSaleResponse, SalesSummaryResponse, StoreListResponse, TestData } from 'src/app/interface/interfaces';
import { ApiService } from 'src/app/store/reports/reports.service';
import { StoresService } from 'src/app/store/stores/stores.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent {


  data:any;
  salesSummary!: SalesSummaryResponse;
  Stores:any;
  productData!:ProductWiseResponse;
  returnsData!:ReturnSaleResponse;
  tableData: TestData[] = [
  ]
  parameters = {
    productType:false,
    storeId:'',
    SalesDate:null,
    Period:'',
    isExportRequest:false
  }
  StoreResponse!:StoreListResponse
  constructor(
    public router: Router,
    private api: ApiService,
    private storesApi:StoresService
  )  {
    this.fetchSalesData()
    this.fetchStores()
  }

  rowSelected(ReferenceData: any) {
    this.router.navigate(['/merchant/allcustomers/customer', ReferenceData.customerId]);
  }

  fetchSalesData(){
    this.api.fetchData(this.api.APIURL,'report/getSalesSummary').subscribe(
      (res)=>{
       this.salesSummary = res
      }
    )
  }
  fetchProductData(){
    this.api.fetchData(this.api.APIURL,'report/getProductWiseReport').subscribe(
      (res)=>{
       this.productData = res
      }
    )
  }
  fetchReturnData(){
    this.api.fetchData(this.api.APIURL,'report/getReturnSaleReport').subscribe(
      (res)=>{
       this.returnsData = res
      }
    )
  }
  
  downloadReport(data:string){
    const url = `report/${data}`
      this.api.fetchData(this.api.APIURL, url).subscribe(
        (res)=>{
          //handle file
        }
      )
  }
  switchTabs(data:any) {
    switch(data.index){
        case 0:
          this.fetchSalesData()
          break
        case 1:
          this.fetchProductData()
          break
        case 2:
          this.fetchReturnData()
          break
    }
}

productType(data:boolean){
    this.parameters.productType = data
}
fetchStores(){
  this.storesApi.fetchData('stores/getStoresByMerchant', {merchantId:1}).subscribe(
    res=>{
      this.StoreResponse = res
    }
  )
}

}
