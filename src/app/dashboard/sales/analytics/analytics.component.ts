import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';

import { ReturnDataAnalytics, SalesAnalyticsResponse, SalesInvoiceData, StoreListResponse } from 'src/app/interface/interfaces';
import { ApiService } from 'src/app/store/reports/reports.service';
import { StoresService } from 'src/app/store/stores/stores.service';

import * as Highcharts from 'highcharts';
import { NotificationService } from 'src/app/utils/notification.service';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit{

  @ViewChild('chart', { static: true })
  chartEl!: ElementRef;
  analyticsData!:SalesAnalyticsResponse;
  StoreResponse!:StoreListResponse;
  returnsData!:ReturnDataAnalytics;
  invoiceData!:SalesInvoiceData
  chartOptions:any;
  PeriodType:string= 'Week';
  indexId:any;
  constructor(
    private api: ApiService,
    private storesApi:StoresService,
    private notify: NotificationService
  ){
    
  }
  ngOnInit(): void {
    this.fetchData();
    this.fetchStores()
    this.plotGraph()
  }

plotGraph()
{
  this.chartOptions = {
    chart:{
      type:'line'
    },
    title:{
      text:'Sales'
    },
    xAxis:{
      categories:['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'],
      title:{
        text:'Day of Week'
      }
    },
    yAxis:{
      title:{
        text:'Amount'
      }
    },
    series:[{
      name:'Amount',
      type:'line',
      data: [500,600,700,800,900,1000,10000]
    }]
  };
}


    fetchData(){
      this.api.fetchDataParams('report/getSalesTrendAnalytic',{PeriodType:this.PeriodType}).subscribe(
        res=>{
          this.analyticsData = res
          this.convertData(this.analyticsData.data.salesGraphs)
        }
      )
    }
    fetchReturnsData(){
      this.api.fetchDataParams('report/getReturnAnalytic',{PeriodType:this.PeriodType}).subscribe(
        res=>{
          this.returnsData = res
          this.convertReturnData(this.returnsData.data.returnGraph)
        }
      )
    }
    fetchInvoiceData(){
      this.api.fetchDataParams('report/getSalesInvoiceAnalytic',{PeriodType:this.PeriodType}).subscribe(
        res=>{
          this.invoiceData = res
          this.convertInvoiceData(this.invoiceData.data.invoiceGraph)
        }
      )
    }
    drawChart() {
      Highcharts.chart('chartContainer', this.chartOptions);
    }
    convertData(value:any){
      const period = value.map((graph:any) => graph.periodType);
      const values = value.map((graph:any) => graph.totalQuantitySold);
      this.chartOptions.xAxis.categories = period;
      this.chartOptions.series[0].data = values;
      this.drawChart()
    }
    convertReturnData(value:any){
      const period = value.map((graph:any) => graph.periodType);
      const values = value.map((graph:any) => graph.returnAmount);
      this.chartOptions.xAxis.categories = period;
      this.chartOptions.series[0].data = values;
      setTimeout(() => {
         this.drawChart()
      }, 1500); 
    }
    convertInvoiceData(value:any){
      const period = value.map((graph:any) => graph.periodType);
      const values = value.map((graph:any) => graph.totalAMount);
      this.chartOptions.xAxis.categories = period;
      this.chartOptions.series[0].data = values;
      setTimeout(() => {
         this.drawChart()
      }, 1500); 
    }

    fetchStores(){
      this.storesApi.fetchData('stores/getStoresByMerchant', {merchantId:1}).subscribe(
        res=>{
          this.StoreResponse = res
        }
      )
    }
    setPeriod(data:string){
     this.PeriodType = data
     this.notify.showInfo('Reloading data with period')
      if(this.indexId == 0 ){
        this.fetchData()
      }else if(this.indexId == 1){
        this.fetchReturnsData()
      }else{
        this.fetchInvoiceData()
      }
    }

    switchTabs(data:any) {
      this.indexId = data
      this.PeriodType = 'Week'
      switch(data.index){
          case 0:
            this.fetchData()
            break
          case 1:
            this.fetchReturnsData()
            break
          case 2:
            this.fetchInvoiceData()
            break
      }
    }
}
