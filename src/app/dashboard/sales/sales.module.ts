import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalyticsComponent } from './analytics/analytics.component';
import { ReportsComponent } from './reports/reports.component';
import { RouterModule, Routes } from '@angular/router';
import { IconsModule, SharedModule } from 'src/app/shared';
import {MatTabsModule} from '@angular/material/tabs';
import { HighchartsChartModule } from 'highcharts-angular';
import { ReturnComponent } from './return/return.component';
import { FormsModule } from '@angular/forms';


const salesRoutes:Routes = [
    {
      path:'',
      pathMatch:'full',
      component:ReportsComponent
    },
    {
      path:'analytics',
      component:AnalyticsComponent
    },
    {
      path:'reports',
      component:ReportsComponent
    },
    {
      path:'sales-return',
      component:ReturnComponent
    }
]

@NgModule({
  declarations: [
    AnalyticsComponent,
    ReportsComponent,
    ReturnComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(salesRoutes),
    IconsModule,
    SharedModule,
    MatTabsModule,
    HighchartsChartModule,
    FormsModule
  ]
})
export class SalesModule { }
