import {Component} from '@angular/core';
import {FormBuilder, Validators,FormGroup} from '@angular/forms';
import { CategoryResponse } from 'src/app/interface/interfaces';
import { HelperService } from 'src/app/services/helper.service';
import { ProductsService } from 'src/app/store/products/products.service';
import { NotificationService } from 'src/app/utils/notification.service';

declare var $: any;
@Component({
    selector: 'app-add-product',
    templateUrl: './add-product.component.html',
    styleUrls: ['./add-product.component.scss']
})


export class AddProductComponent {
  isLinear = false;
  isSaveinDraft:boolean=false;
  isPublishDeal:boolean=false;
  url : any;
  Categories!: CategoryResponse;
  Form_AddUser!:FormGroup
  ProductForm!: FormGroup;
  ImageList:any = []
  imageListn:any = []
  pageNumber:number = 1;
  TagList:any = []
  textValue:string= '';
  isProcessing:boolean = false;
  taxesData:any;
  returnPolicy:any
  selectedPolicy!:string;
  constructor(private _formBuilder: FormBuilder, public api: ProductsService ,
    public notify:NotificationService,
    private helper:HelperService) {}
    ngOnInit()
    {
      this.ProductForm = this._formBuilder.group({
        length:[''],
        breadth:[''],
        height:[''],
        productName:['', [Validators.required]],
        categoryId:['',[Validators.required]],
        description:['',[Validators.required]],
        weight:['',[Validators.required]],
        sku:['',[Validators.required]],
        brandName:[''],
        originalPrice: [''],
        salesPrice: [''],
        discountPrice: null,
        availableUnits: [''],
        productTags:[''],
        //form 2
        subCategoryId:[null],
        productVariants:[],
        //form 3
        mpn:[''],
        barcode: [''],
        createdBy:['sdjsldklssdnei398fhask'],
        otherTermsAndCondition: [''],
        warranty:[''],
        rewardPercentage: [''],
        unit: [''],
        storeId: 1,
        isLowStockAlertEnabled: true,
        lowStockAlertValue: [''],
        productionDate: [''],
        expiryDate: [''],
        discountId: null,
        isDiscounted: false,
        isLooseProduct: false,
        hasVariant: false,
        images:[],
        //arr vals
        merchantId: [1],
        policyId: [''],
        policyDescription:[''],
        taxId: [''],
      })

      this.initiateValues()
     
    }

    initiateValues(){
      this.fetchCategories()
      this.fetchTaxes()
      this.fetchPolicies()
    }

  onSelectFile(event:any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event) => { // called once readAsDataURL is completed
        this.url = event.target?.result;
      }
    }
  }

  fetchCategories(){
    this.api.fetchData('category/getCategories?', {merchantId:1}).subscribe(
      res=>{
        this.Categories = res
      }
    )
  }
  drop(ev: any) {
    ev.preventDefault();
    let file: any;
    if (ev.dataTransfer.items) {
        let item = ev.dataTransfer.items[0];
        if (item.kind === 'file') {
            file = item.getAsFile();
        }
    } else {
        file = ev.dataTransfer.files[0];
    }
  }

SetImages()
{
this.imageListn.forEach((element:any) => {
    this.ImageList.push({
        image:element.Content,
        fileName:element.Name,
        isDefault:element == this.imageListn[0] ? true:false
    })
  });
}

getElementStatus(page: number): string {
  if (page === this.pageNumber) {
    return 'current';
  } else if (page < this.pageNumber) {
    return 'past';
  } else {
    return 'future';
  }
}
 


  submitProduct(){
    this.isProcessing = !this.isProcessing
    let body = {
      ...this.ProductForm.value,
      images:this.ImageList,
      productTags:this.TagList,
      expirationDate:this.helper.DateToIso(this.ProductForm.value.expiryDate),
      productionDate:this.helper.DateToIso(this.ProductForm.value.productionDate),
    }
   this.api.postData('product/addProduct',body).subscribe(
    res=>{
      if(res){
        this.notify.showSuccess(res.responseMessage)
        setTimeout(() => {
          this.helper.router.navigate(['/products-catalog'])
        }, 1500);
      }else{
        this.notify.showError(res.responseMessage)
      }
    }
   )
  }

  onEnterKey(value: string): void {
    this.TagList.push(value);
    this.textValue = '';
  }
  removeTag(value:number):void{
    if (value == undefined) {
      this.TagList = [];
    } else {
      this.TagList.splice(value, 1);
    }
  }


  fileChangeEvent(event: any): void {
    if(this.imageListn.length > 2){
      this.notify.showError('Maximum of 3 images allowed')
    return 
    }
    let file = event.target.files[0];
    let accepted = [
      'image/png',
      'image/jpeg'
    ]
    if(!accepted.includes(file.type)){
      this.notify.showError('Only PNG & JPG types allowed')
      return 
    }
    if(file.size > 400000){
      this.notify.showError('Maximum size is 400kb')
      return 
    }
    let base64result:any;
    let reader:any = new FileReader();
    reader.onloadend = function() {
      base64result = reader.result;
    }
    reader.readAsDataURL(file);
    setTimeout(() => {
      this.imageListn.push({
          base64:base64result,
          Name:file.name,
          Size:file.size,
          Extension: reader.result.split(';')[0].split('/')[1],
          Content: base64result.split(',')[1],
          OriginalContent:base64result
      })
    }, 900); 
  }


  switchPage(){
    switch(this.pageNumber)
    {
      case 1:
        this.pageNumber++ 
        this.SetImages();
      break
      case 2:
       this.pageNumber++ 
      break
      case 3:
        this.pageNumber++ 
      break
      case 4:
        this.pageNumber++ 
      break
      case 5:
        this.submitProduct()
        break;
    }
    //TODO::this.EvaluateButton()
  }

  removeImage(index: number) {
    if (index == undefined) {
      this.imageListn = [];
    } else {
      this.imageListn.splice(index, 1);
    }
  }
  fetchTaxes(){
    this.api.fetchData('taxManagement/getTaxes',{merchantId:1}).subscribe(
      res=>{
        this.taxesData= res.data
      }
    )
  }
  fetchPolicies(){
    this.api.fetchData('policies/getPolicies',{merchantId:1}).subscribe(
      res=>{
        this.returnPolicy= res.data
      }
    )
  }
  selectPolicy(data:any){
    let parsedSource = JSON.parse(data.target.value)
    this.ProductForm.get('policyId')?.setValue(Number(parsedSource.policyId))
    this.ProductForm.get('policyDescription')?.setValue(data.termsAndCondition);
    this.ProductForm.get('policyDescription')?.disable();
  }

  Stringify(data:any){ 
    return JSON.stringify(data) 
  }
}
