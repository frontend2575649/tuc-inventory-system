import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { FeatherModule } from 'angular-feather';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';
import { AddProductComponent } from './add-product.component';
import { MatIconModule } from '@angular/material/icon';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { MatCardModule } from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from "@angular/material/form-field";
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { TagInputModule } from 'ngx-chips';
import { SharedModule } from 'src/app/shared';
const routes: Routes = [
    {
        path: "",
        component: AddProductComponent,

    }
  ];
  const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 3,
};
  @NgModule({
    declarations: [AddProductComponent],
    imports: [
      CommonModule,InputFileModule,SharedModule,
      FeatherModule,MatCardModule,TagInputModule,
      NgSelectModule,FormsModule,ReactiveFormsModule,MatInputModule,
      MatStepperModule,MatFormFieldModule,
      RouterModule.forChild(routes),
      // ZXingScannerModule
    ]
  })
export class AddProductModule { }
