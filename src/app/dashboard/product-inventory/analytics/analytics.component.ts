import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ProductGraphData, StockUpdateData, StoreListResponse } from 'src/app/interface/interfaces';
import { StoresService } from 'src/app/store/stores/stores.service';
import { NotificationService } from 'src/app/utils/notification.service';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {

  @ViewChild('chart', { static: true })
  chartEl!: ElementRef;
  isStockUpdate:boolean = false
  StoreResponse!:StoreListResponse;
  AnalyicsGraphResponse!:ProductGraphData;
  StockUpdateData!: StockUpdateData
  PeriodType:string= 'Week';
  indexId= 0;
  chartOptions:any;
  ngOnInit(): void {
    this.fetchProductData()
    this.plotGraph()
  }

  constructor(
    private storesApi:StoresService,
    private notify: NotificationService
  ){}

switchTabs(data:any) {
  this.indexId = data.index
  switch(this.indexId){
      case 0:
        this.isStockUpdate = false
        this.fetchProductData()
        break
      case 1:
        this.isStockUpdate = true
        this.fetchStockData()
        break
  }
}

fetchStores(){
  this.storesApi.fetchData('stores/getStoresByMerchant', {merchantId:3}).subscribe(
    res=>{
      this.StoreResponse = res
    }
  )
}


fetchStockData(){
  this.storesApi.fetchData('report/getStockProductReportGraph',{PeriodType:this.PeriodType}).subscribe(
    (res)=>{
     this.StockUpdateData = res
     this.convertData(this.StockUpdateData.data.stockUpdateGraphs)
    }
  )
}

fetchProductData(){
  this.storesApi.fetchData('report/getProductInventoryReportGraph',{PeriodType:this.PeriodType}).subscribe(
    (res)=>{
     this.AnalyicsGraphResponse = res
     this.convertReturnData(this.AnalyicsGraphResponse.data.productInventoryGraphs)
    }
  )
}
setPeriod(data:string){
  this.PeriodType = data
  this.notify.showInfo('Reloading data with period')
   if(this.indexId == 0){
    this.fetchProductData()
   }else{
    this.fetchStockData()
   }
 }


  convertData(value:any){
    const period = value.map((graph:any) => graph.periodType);
    const values = value.map((graph:any) => graph.totalQuantitySold);
    this.chartOptions.xAxis.categories = period;
    this.chartOptions.series[0].data = values;
    this.drawChart()
  }

  convertReturnData(value:any){
    const period = value.map((graph:any) => graph.periodType);
    const values = value.map((graph:any) => graph.availableStock);
    this.chartOptions.xAxis.categories = period;
    this.chartOptions.series[0].data = values;
    setTimeout(() => {
      this.drawChart()
    }, 1500); 
  }
  drawChart() {
    Highcharts.chart('chartContainer', this.chartOptions);
  }

plotGraph(){
  this.chartOptions = {
    chart:{
      type:'line'
    },
    title:{
      text:'Sales'
    },
    xAxis:{
      categories:['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'],
      title:{
        text:'Day of Week'
      }
    },
    yAxis:{
      title:{
        text:'Amount'
      }
    },
    series:[{
      name:'Amount',
      type:'line',
      data: [500,600,700,800,900,1000,10000]
    }]
  };
}
}
