import { Component, OnInit } from '@angular/core';
import { ProductInventoryResponse, StockUpdateResponse, StoreListResponse, TestData } from 'src/app/interface/interfaces';
import { ApiService } from 'src/app/store/reports/reports.service';
import { StoresService } from 'src/app/store/stores/stores.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  productReports!: ProductInventoryResponse;
  Stores:any;
  stockUpdate!: StockUpdateResponse;
  StoreResponse!:StoreListResponse
  constructor(
    private api: ApiService,
    private storesApi:StoresService
  ){}

  tableData: TestData[] = []

  ngOnInit(): void {
    this.fetchProductsData()
    this.fetchStockUpdate()
    this.fetchStores()
  }

  fetchProductsData(){
    this.api.fetchData(this.api.APIURL,'report/getProductInventory').subscribe(
      (res:ProductInventoryResponse)=>{
       this.productReports = res
      }
    )
  }
  fetchStockUpdate(){
    this.api.fetchData(this.api.APIURL, 'report/getStockUpdateReport').subscribe(
      (res:StockUpdateResponse)=>{
        this.stockUpdate = res
      }
    )
  }
  fetchStores(){
    this.storesApi.fetchData('stores/getStoresByMerchant', {merchantId:1}).subscribe(
      res=>{
        this.StoreResponse = res
      }
    )
  }
}
