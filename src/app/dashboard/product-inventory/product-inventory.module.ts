import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports/reports.component';
import { AnalyticsComponent } from './analytics/analytics.component';

import { RouterModule, Routes } from '@angular/router';
import { IconsModule, SharedModule } from 'src/app/shared';
import {MatTabsModule} from '@angular/material/tabs';
import { HighchartsChartModule } from 'highcharts-angular';


const InventoryRoutes:Routes = [
    {
      path:'',
      pathMatch:'full',
      component:ReportsComponent
    },
    {
      path:'analytics',
      component:AnalyticsComponent
    },
    {
      path:'reports',
      component:ReportsComponent
    }
]


@NgModule({
  declarations: [
    ReportsComponent,
    AnalyticsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(InventoryRoutes),
    IconsModule,
    SharedModule,
    MatTabsModule,
    HighchartsChartModule
  ]
})
export class ProductInventoryModule { }
