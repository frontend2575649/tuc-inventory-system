import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SettingsService } from 'src/app/store/settings/settings.service';
import { NotificationService } from 'src/app/utils/notification.service';

@Component({
  selector: 'app-discount-modal',
  templateUrl: './discount-modal.component.html',
  styleUrls: ['./discount-modal.component.scss']
})
export class DiscountModalComponent {


  modalData:any;
  view:string = '';
  checkedItemIds: number[] = [];
  searchText:string= '';
  //Demo products items till  API is released
  items: any[] = [
    { id: 1, name: 'Coca Cola', checked: false },
    { id: 2, name: 'Salad', checked: false },
    { id: 3, name: 'Pepsi', checked: false }
  ];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
              public api:SettingsService, public notify:NotificationService,
              public dialogRef: MatDialogRef<DiscountModalComponent>)  {
    this.modalData = data;
    this.view = this.modalData.action;
  }

  switchView(data:string){
    switch(data){
        case 'skipDelete':
          this.view = 'ConfirmDelete'
          break;
        case 'delete':
          this.deleteDiscount()
          break
        case 'confirm':
          this.assignProducts()
          break
        default:
          this.dialogRef.close()
          break;
    }
  }

  deleteDiscount(){
    this.api.deleteData('discount/deleteDiscount',
        {merchantId:this.modalData.body.merchantId, discountId:this.modalData.body.discountId}).subscribe(
      res=>{
        if(res.responseCode == 200){
          this.notify.showMessage(res.responseMessage)
        }else{
          this.notify.showError(res.responseMessage)
        }
      }
    )
  }
  

  toggleChecked(itemId: number) {
    if (this.checkedItemIds.includes(itemId)) {
      this.checkedItemIds = this.checkedItemIds.filter(id => id !== itemId);
    } else {
      this.checkedItemIds.push(itemId);
    }
  }

  assignProducts(){
    let body = {
      merchantId: this.modalData.body.merchantId,
      discountId: this.modalData.body.discountId,
      productIds:this.checkedItemIds
    }
    this.assignItem(body)
}

assignCategories(){
  let body = {
    merchantId: this.modalData.body.merchantId,
    discountId: this.modalData.body.discountId,
    categoriesIds:this.checkedItemIds
  }
  this.assignItem(body)
}

assignItem(body:any){
  const url = this.modalData.action == 'Assign' ? 'discount/applyOnProducts' : 'discount/applyOnCategories';
    this.api.postData(url, body).subscribe(
      res=>{
        if(res){
          this.notify.showSuccess(res.responseMessage)
          setTimeout(() => {
            window.location.reload()
          }, 1500);
        }else{
          this.notify.showError(res.responseMessage)
        }
      }
    )
  }
  fetchItems(){
    //::Fetch Product or Category this.api.fetchData()
  }



}
