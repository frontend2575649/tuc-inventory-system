import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { DiscountResponse, StoreListResponse } from 'src/app/interface/interfaces';
import { HelperService } from 'src/app/services/helper.service';
import { SettingsService } from 'src/app/store/settings/settings.service';
import { NotificationService } from 'src/app/utils/notification.service';

@Component({
  selector: 'app-edit-discount',
  templateUrl: './edit-discount.component.html',
  styleUrls: ['./edit-discount.component.scss']
})
export class EditDiscountComponent implements OnInit{

  DiscountForm!: FormGroup;
  isAllStores: boolean = false;
  isAllTime: boolean = false;
  DiscountDetails:any;
  storesList:any;
  selectedDays: any = [];
  StoreResponse!:StoreListResponse;
  ScheduledDays = [
    {
      key:'Sunday',
      value:'S'
    },
    {
      key:'Monday',
      value:'M'
    },{
      key:'Tuesday',
      value:'T'
    },{
      key:'Wednesday',
      value:'W'
    },{
      key:'Thursday',
      value:'T'
    },{
      key:'Friday',
      value:'F'
    },
    {
      key:'Saturday',
      value:'S'
    },
  ];
  constructor(
    private fb:FormBuilder,
    public api : SettingsService,
    private notify: NotificationService,
    public helper: HelperService,
    private route: ActivatedRoute
  ){
  }

  ngOnInit(): void {
    this.initiateForm()

    this.route.params.subscribe((params:any) => {
      this.GetDiscount(params.discountId)
    });

  }

  initiateForm(){
    this.DiscountForm = this.fb.group({
      name: ['', [Validators.required]],
      posName: ['',[Validators.required]],
      description:['',[Validators.required]],
      startDate:['',Validators.required],
      endDate: ['',[Validators.required]],
      startTime:['',[Validators.required]],
      endTime:['',[Validators.required]],
      type:['',[Validators.required]],
      StoreListing:[''],
      isAllStores:[false]
     })
  }

  GetDiscount(data:number){
      this.api.fetchData('discount/getDiscount',{discountId:data}).subscribe(
      res=>{
        if(res){
          this.DiscountDetails = res
          this.setDiscountForm(this.DiscountDetails.data)
        }
      }
    )}


  setDiscountForm(data:any){
      this.DiscountForm.controls['name'].setValue(data.name)
      this.DiscountForm.controls['posName'].setValue(data.posName)
      this.DiscountForm.controls['description'].setValue(data.description)
      this.DiscountForm.controls['startDate'].setValue(this.helper.DateStringToFull(data.startDate))
      this.DiscountForm.controls['endDate'].setValue(this.helper.DateStringToFull(data.endDate))
      this.DiscountForm.controls['type'].setValue(data.type)
      this.isAllTime = data.runAllTime
      if(!data.runAllTime){
        this.DiscountForm.controls['startTime'].setValue(data.startTime)
        this.DiscountForm.controls['endTime'].setValue(data.endTime)
      }
      this.isAllStores = data.stores.length > 0 ? true : false;
      this.DiscountForm.controls['isAllStores'].setValue(this.isAllStores)
      if(data.allStores){
        this.selectedDays.push(data)
      }
  }
  

  returnHome(){}
  checkStores
  (){}
  updateDiscount(data:any){
    let body = {
      ...data,
      merchantId: 1,
      discountId:this.DiscountDetails.data.id
    }
    console.log(body)
    this.api.putData('discount/editDiscount', data).subscribe(
      res=>{
       if(res){
          this.notify.showMessage(res.responseMessage)
       }else{
        this.notify.showError(res.responseMessage)
       }
      },
      err=>{
        this.notify.showError('Unable to complete that')
      }
    )
  }
  checkTime(){}
  countryChange(eevnt: any){}

  fetchStores(){
    this.api.fetchData('stores/getStoresByMerchant',{merchantId:1}).subscribe(
      res=>{
        this.storesList = res
      }
    )
  }

  ToggleDay(data:any)
  {
    if(this.selectedDays.includes(data))
    {
      const index = this.selectedDays.indexOf(data);
      this.selectedDays.splice(index, 1);
    }else{
      this.selectedDays.push(data)
    }
  }
}
