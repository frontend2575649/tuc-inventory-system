import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StoreListResponse } from 'src/app/interface/interfaces';
import { BaseService } from 'src/app/services/base.service';
import { HelperService } from 'src/app/services/helper.service';
import { SettingsService } from 'src/app/store/settings/settings.service';
import { NotificationService } from 'src/app/utils/notification.service';

@Component({
  selector: 'app-add-discount',
  templateUrl: './add-discount.component.html',
  styleUrls: ['./add-discount.component.scss']
})
export class AddDiscountComponent implements OnInit {

  DiscountForm!: FormGroup;
  isAllStores: boolean = false;
  isAllTime: boolean = false;
  selectedStores: any = [];
  selectedDays: any = [];
  StoreResponse!:StoreListResponse;
  ScheduledDays = [
    {
      key:'Sunday',
      value:'S'
    },
    {
      key:'Monday',
      value:'M'
    },{
      key:'Tuesday',
      value:'T'
    },{
      key:'Wednesday',
      value:'W'
    },{
      key:'Thursday',
      value:'T'
    },{
      key:'Friday',
      value:'F'
    },
    {
      key:'Saturday',
      value:'S'
    },
  ];
  StoreListings:any = []
  constructor(
    private fb:FormBuilder,
    public api : SettingsService,
    private notify: NotificationService,
    public helper: HelperService,
  ){
  }

  ngOnInit(): void {
    this.DiscountForm = this.fb.group({
        name: ['', [Validators.required]],
        posName: ['',[Validators.required]],
        description:['',[Validators.required]],
        startDate:['',Validators.required],
        endDate: ['',[Validators.required]],
        startTime:['',[Validators.required]],
        endTime:['',[Validators.required]],
        type:['',[Validators.required]],
        StoreListing:['']
    })
    this.fetchStores()
  }

  createDiscount(data:any){
    let body = {
      ...data,
      merchantId:1,
      startDate:this.helper.DateToIso(data.startDate),
      endDate:this.helper.DateToIso(data.endDate),
      startTime:this.helper.DateToIso(data.startDate),
      endTime:this.helper.DateToIso(data.endDate),
      allStores: this.isAllStores,
      runAllTime: this.isAllTime,
      storeIds:this.isAllStores ? [] : this.DiscountForm.value.StoreListings,
      daysToRun:this.isAllTime ? [] : this.selectedDays,
    }
    this.api.postData('discount/createDiscount',body).subscribe(
      res=>{
        if(res){
        this.notify.showMessage(res.responseMessage)
        this.helper.router.navigate(['/settings/discounts'])
        }else{
          this.notify.showError('Unable to create discount')
        }
      }
    )
  }

  checkStores(){
    this.isAllStores = !this.isAllStores
  }
  checkTime(){
    this.isAllTime = !this.isAllTime
  }
  returnHome(){
    this.helper.router.navigate(['/settings/discounts'])
  }

  ToggleDay(data:any)
  {
    if(this.selectedDays.includes(data))
    {
      const index = this.selectedDays.indexOf(data);
      this.selectedDays.splice(index, 1);
    }else{
      this.selectedDays.push(data)
    }
  }

  fetchStores(){
    this.api.fetchData('stores/getStoresByMerchant', {merchantId:1}).subscribe(
      res=>{
        this.StoreResponse = res
        for (let index = 0; index < this.StoreResponse.data.length; index++) {
          const element: any = this.StoreResponse.data[index];
          this.StoreListings = [...this.StoreListings, {
              id: element.storeId,
              key: element.merchantId,
              text: element.storeName
          }];
        }
      }
    )
  }
  countryChange(event:any){}
}
