import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DiscountResponse, TestData } from 'src/app/interface/interfaces';
import { HelperService } from 'src/app/services/helper.service';
import { SettingsService } from 'src/app/store/settings/settings.service';
import { DiscountModalComponent } from './discount-modal/discount-modal.component';

@Component({
  selector: 'app-discounts',
  templateUrl: './discounts.component.html',
  styleUrls: ['./discounts.component.scss']
})
export class DiscountsComponent implements OnInit {

  @ViewChild(DiscountModalComponent) discountModal!: DiscountModalComponent;

  discountData!:DiscountResponse;

  constructor(public dialog: MatDialog, private api: SettingsService,
          public helper: HelperService, private router: Router){
  }
  tableData: TestData[] = []

  ngOnInit(): void {
    this.fetchDiscounts()
  }

    openDialog(data:string, body?:any){
      this.dialog.open(DiscountModalComponent, {
        width: '500px',
        data:{action:data,body:{merchantId:1,discountId:body?.discountId}}
      })
    }
    fetchDiscounts(){
      const params = { merchantId: 1};
      this.api.fetchData('discount/getDiscounts',params).subscribe(
        (res)=>{
         this.discountData = res
        }
      )
    }

    EditDiscount(data:number){
      this.router.navigate(['settings/edit-discount',data]);
    }
}
