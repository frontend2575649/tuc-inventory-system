import { Component, OnInit } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Observable, tap, } from 'rxjs';
import { EnableNotificationReqBody, Store, StoreListResponse, TaxOnProduct } from 'src/app/interface/interfaces';
import { CashierService } from 'src/app/services/cashier.service';
import { GeneralSettingsService } from 'src/app/services/generalSettings.service';
import { NotificationService } from 'src/app/services/notification.service';
// import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'app-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.scss'],

})
export class GeneralSettingsComponent implements OnInit {
  enableTrackingForStock: boolean = false;
  userPreviouslySelectedStockTracking!: boolean;
  enableTaxes: boolean = false;
  generalNotifications: EnableNotificationReqBody = {
    enableEmailAlert: false,
    enableNotificationForStockAlert: false,
    enableProductReturnAlert: false,
    autoGenerateCreditNote: false
  }
  storesOfThisMerchant$!: Observable<StoreListResponse>;
  selectedStores: any[] = [];
  rotate: boolean = false;
  taxes!: TaxOnProduct
   constructor(private cashierService: CashierService, private generalSettingsService: GeneralSettingsService, private notifcation: NotificationService){
    this.getPreviousSettings = this.getPreviousSettings.bind(this);
    this.getStores = this.getStores.bind(this);
   }
   
   ngOnInit(): void {
     this.getStores();
     this.getPreviousSettings();
     this.getPreviousTaxAndStockSettings();
   }

   getPreviousSettings(){
    // this.loa
     this.generalSettingsService.getPreviousSettings('1')
     .pipe(tap(({data}) => {
      this.generalNotifications = {...data};
     }))
     .subscribe()
   }

   getStores(){
    this.storesOfThisMerchant$ = this.cashierService.getStoreList('1').pipe(tap(({data}) => this.selectedStores = data.map(elem => elem.isTracked ? elem.storeId: null).filter(element => element != null)));
   }

   getPreviousTaxAndStockSettings(){
    this.generalSettingsService.taxAndStockSettings(1)
    .pipe(tap(({data}) => {
      this.enableTrackingForStock = data.enableStockTracking;
      this.enableTaxes = data.enableTaxOnProduct;
      this.taxes = data.taxInfo;
      this.userPreviouslySelectedStockTracking = this.enableTrackingForStock;
     }))
    .subscribe()
   }

   addToSelectedStores(event: MatCheckboxChange, store: Store){
     if(event.checked){
      this.selectedStores.push(store.storeId);
      return;
     }
     const found = this.selectedStores.indexOf(store.storeId);
     this.selectedStores.splice(found, 1);
   }

   startTracking(event: MatSlideToggleChange){
    debugger
    if(this.userPreviouslySelectedStockTracking && this.selectedStores.length > 0){
      this.selectedStores = [];
      this.userPreviouslySelectedStockTracking = false;
      this.enableOrDisableTracking(event.source.checked, []);
      return;
    }
    if(!event.checked && this.selectedStores.length > 0){
       this.enableOrDisableTracking(event.source.checked, []);
       return;
    }
    else if(this.selectedStores.length < 1){
      this.notifcation.modifiedNotificationService(
        {
      icon: 'warning',
      title: "Warning!!!",
       text: 'If You intend to enable tracking, you must select stores!',
       didClose: () => event.source.toggle(),
      }); 
      return
    }
    this.enableOrDisableTracking(event.checked, this.selectedStores);
      
   }

   enableOrDisableTracking(enableTracking: boolean, storeIdsToUse: Array<any>){
    this.generalSettingsService.enableTracking({
        merchantId: 1,
        storeIds: this.selectedStores,
        status: enableTracking
      }).subscribe(
        {
          next: val => {
            if(val.responseCode == 200){
              this.notifcation.modifiedNotificationService(
                {
              icon: 'success',
              title: "Success!!!",
              text: 'Settings Updated Successfully!',
              didClose: () => {this.getPreviousSettings(); this.getStores()}
             })
            }
          },
          error: err => {
            this.notifcation.modifiedNotificationService(
              {
            icon: 'error',
            title: "Error!!!",
            text: `Couldn't update Settings!.`,
          })
          }
        }
      )
   }

   startTaxing(event: MatSlideToggleChange){
    if(!event.checked && this.taxes){
      this.enableTaxing(event.source.checked);
      return;
    }
    else if(!this.taxes){
      this.notifcation.modifiedNotificationService({
        icon: 'error',
        title: 'Wrong Input',
        text: `You didn't select any type of tax!`,
        didClose: () => event.source.toggle(),
      });
      return
    }
    this.enableTaxing(true);
    }

 enableTaxing(enableOrDisable: boolean,){
  this.generalSettingsService.enableTaxingOnProduct({
    merchantId: 1,
    enableTaxOnProduct: enableOrDisable,
    taxInfo: this.taxes
  }).subscribe(
    {
      next: val => {
        if(val.responseCode == 200){
          this.notifcation.modifiedNotificationService(
            {
          icon: 'success',
          title: "Success!!!",
          text: 'Settings Updated Successfully!',
          didClose: () => this.getPreviousSettings()
        })
        }
      },
      error: err => {
        this.notifcation.modifiedNotificationService(
          {
        icon: 'error',
        title: "Error!!!",
        text: `Couldn't update Settings!.`,
      })
      }
    }
  )
 }



 enableNotification(event: MatSlideToggleChange, type: keyof EnableNotificationReqBody){
   this.generalSettingsService.enableNotification('1', {...this.generalNotifications, [type]: event.checked})
   .subscribe({
    next: val => {
      if(val.responseCode == 200){
        this.notifcation.modifiedNotificationService(
          {
        icon: 'success',
        title: "Success!!!",
        text: val.responseMessage as string,
        didClose: () => this.getPreviousSettings()
      })
      }
    },
    error: err => {
      this.notifcation.modifiedNotificationService(
        {
      icon: 'error',
      title: "Error!!!",
      text: `Couldn't update Settings!.`,
    })
    }
   })
 }
}
