import { Component, OnInit } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import * as Feather from 'feather-icons';


@Component({
  selector: 'app-page-tips',
  templateUrl: './page-tips.component.html',
  styleUrls: ['./page-tips.component.scss']
})
export class PageTipsComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<PageTipsComponent>) {

  }



  ngOnInit(): void {
    Feather.replace();

  }
  close() {
    this.dialogRef.close();
  }
}
