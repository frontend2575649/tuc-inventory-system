import { Component, OnInit } from '@angular/core';
import { PageTipsComponent } from './page-tips/page-tips.component';
import {
  MatDialog,
} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { Observable, lastValueFrom } from 'rxjs';
import { ACategoryFromStore, CreatedPolicy, Product } from 'src/app/interface/interfaces';
import { ReturnPoliciesService } from 'src/app/services/return-policies.service';
import { map } from 'rxjs';
import { NotificationService } from 'src/app/services/notification.service';


@Component({
  selector: 'app-return-policy',
  templateUrl: './return-policy.component.html',
  styleUrls: ['./return-policy.component.scss']
})
export class ReturnPolicyComponent implements OnInit {
  returnPolicies$!: Observable<CreatedPolicy[]>;
  categories$!: Observable<ACategoryFromStore[]>;
  products$!: Observable<Product[]>;
  editPolicy: boolean = false;
  currentPolicyToBeEdit!: any;
  productsForPolicyApplication: number[] = [];
  productsFromAPolicy: Product[] =  []
  addAPolicy = {
    merchantId: 1,
    name: '',
    termAndConditions: '',
    charges: 0,
    isDefault: false,
    productReturnDays: 0,
    productIds: '',
    categories: ''
  }
  ngOnInit(): void {
    Feather.replace();
    this.getReturnPolicies();
    this.getProducts();
    this.getCategories();
  }
  constructor(
    public _FormBuilder: FormBuilder,
    public dialog: MatDialog,
    private policies: ReturnPoliciesService,
    private notification: NotificationService,
    public router: Router) { 
      this.getReturnPolicies = this.getReturnPolicies.bind(this)
    }

  openDialog() {
    this.dialog.open(PageTipsComponent, {
      panelClass: [
        'customSideBarDialogContainer',
        'animate__animated',
        'animate__slideInRight',
      ],
      width: '470px',
      height: '100vh',
      position: { right: '0' },
      disableClose: true,
    });
  }


  Form_AddReturnPolicy!: FormGroup;
  Form_EditReturnPolicy!: FormGroup;
  Form_AddReturnPolicy_Load() {
    this._FormBuilder.group({
      TaxName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern('^[a-zA-Z \-\']+')])],

    })

  }
  Form_AddReturnPolicy_Process(_FormValue: any) {

  }
  Form_EditReturnPolicy_Load() {
    this._FormBuilder.group({
      TaxName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern('^[a-zA-Z \-\']+')])],

    })

  }
  Form_EditReturnPolicy_Process(_FormValue: any) {}

  getReturnPolicies(){
    this.returnPolicies$ = this.policies.getReturnPolicies('1').pipe(map(({data}) => data))
  }
  getCategories(){
    this.categories$ = this.policies.getCategories('1').pipe(map(({data}) => data))
  } 
  getProducts(){
    this.products$ = this.policies.getProducts({PageNumber: 1, PageSize: 10}).pipe(map(({data}) => data))
  }

  async setPolicyIdForApplication(policy: CreatedPolicy){
    this.currentPolicyToBeEdit = policy.policyId;
    try {
    const {products} = await this.fetchAPolicy(policy);
    this.productsFromAPolicy = products;
    } catch (error) {
      this.notification.modifiedNotificationService({
        icon: 'error',
        title: "Error!!!",
        text: `Products linked to policy couldn't be fetched.`,
      })
    }
    
  }
  addToSelectedProductsForPolicyApplication(event: Event){
    const input = event.target as HTMLInputElement;
    if(input.checked){
      this.productsForPolicyApplication.push(parseInt(input.value));
      return;
    }
    const found = this.productsForPolicyApplication.indexOf(parseInt(input.value));
    this.productsForPolicyApplication.splice(found, 1);
  }

  applyPolicyToSelectedProducts(event: Event){
    const btn = event.target as HTMLButtonElement;
    const prevText = btn.textContent;
    btn.textContent = 'Applying Policy...';
    this.policies.applyPolicyOnAProduct({merchantId: 1, policyId: this.currentPolicyToBeEdit, productds: this.productsForPolicyApplication})
    .subscribe({
      next: val => {
        this.notification.modifiedNotificationService({
          icon: 'success',
          title: "Success!!!",
          text: `Policy applied to product successfully!!.`,
          didClose: ()=> {this.getReturnPolicies()}
        })
        this.closeModal();
        (document.querySelector('.productModal') as HTMLElement)?.click();
        btn.textContent = prevText;
      },
      error: err => {
        this.notification.modifiedNotificationService({
          icon: 'error',
          title: "Error!!!",
          text: `Unable to apply policy to products.`,
        })
      }
    })
  }

  async editAPolicy(policy: CreatedPolicy){
    try {
      const data =  await this.fetchAPolicy(policy);
      const {name, termAndConditions, charges, isDefault, productReturnDays} = data;
      this.addAPolicy = {name, termAndConditions, isDefault, charges, productIds: '', categories: '', productReturnDays, merchantId: 1};
      this.addAPolicy.productIds = data.products.length > 0 ? data.products[0]!.id.toString(): '';
      this.addAPolicy.categories = data.categories.length > 0 ? data.categories[0].id.toString(): '';
      this.editPolicy = true;
      this.currentPolicyToBeEdit = policy.policyId; 
    } catch (error) {
      this.notification.modifiedNotificationService({
        icon: 'error',
        title: "Error!!!",
        text: `Policy couldn't be fetched for update.`,
      })
     }
  }

  async fetchAPolicy(policy: CreatedPolicy){
    const {data} = await lastValueFrom(this.policies.fetchAReturnPolicy(policy.policyId));
    return data;
  }

  savePolicy(event: Event){
    const btn = event.target as HTMLButtonElement;
    const prevText = btn.textContent;
    btn.textContent = 'Saving...';
    if(this.addAPolicy.productIds == '' || this.addAPolicy.categories == ''){
      this.notification.modifiedNotificationService({
          icon: 'error',
          title: "Error!!!",
          text: `Please select a category or product`,
      })
      btn.textContent = prevText;
      return;
    }
    debugger
    const products = [];
    const categories = [];
    products.push(parseInt(this.addAPolicy.productIds));
    categories.push(parseInt(this.addAPolicy.categories));
    this.policies.createAReturnPolicy({...this.addAPolicy, productIds: products, categories: categories})
    .subscribe(
      {
        next: val => {
          this.notification.modifiedNotificationService(
            {
              icon: 'success',
              title: "Success!!!",
              text: `Return Policy Created Successfully.`,
              didClose: ()=> {this.getReturnPolicies()}
            }
          )
          this.dialog.closeAll();
          btn.textContent = prevText;
          this.closeModal();
        },
        error: err => {
          this.notification.modifiedNotificationService(
            {
              icon: 'success',
              title: "Success!!!",
              text: `Failed to create policy.`,
            }
          )
          btn.textContent = prevText;
          this.closeModal();
        }
      }
    )

    
  }

  closeModal(){
    (document.querySelector('.dismissBtn') as HTMLElement)?.click();
    
  }

  deletePolicy(id: any){
    this.policies.deleteAPolicy(1, id)
    .subscribe({
      next: val => {
        this.notification.modifiedNotificationService(
          {
        icon: 'success',
        title: "Success!!!",
        text: 'Return policy deleted Successfully!',
        didClose: () => {this.getReturnPolicies();}
      })
      },
      error: err => {
        this.notification.modifiedNotificationService({
          icon: 'error',
          title: "Error!!!",
          text: 'Unable to delete return policy!',
        })
      }
    })
  }
}
