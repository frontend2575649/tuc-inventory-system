
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { PageTipsComponent } from './page-tips/page-tips.component';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { ACategoryFromStore, AStoreLocation, ATax, ATaxToBeCreated, TaxOnProduct, TestData } from 'src/app/interface/interfaces';
import { Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { Observable, lastValueFrom, map, tap } from 'rxjs';
import { TaxesService } from 'src/app/services/taxes.service';
import { NotificationService } from 'src/app/services/notification.service';
import { ReturnPoliciesService } from 'src/app/services/return-policies.service';

declare var $: any;





@Component({
  selector: 'app-taxes',
  templateUrl: './taxes.component.html',
  styleUrls: ['./taxes.component.scss']
})
export class TaxesComponent implements OnInit {
  taxes$!: Observable<ATax[]>;
  currentTaxToBeEdit!: any;
  locationsOfAMerchant: string[] = ['Lagos', 'Imo','Anambra', 'Abuja', 'Rivers'];
  createATax= {
    name: '',
    rate: 0,
    taxOnProduct: TaxOnProduct.Exclude,
    enableTax: false,
    locationsName: '',
  }
  edit: boolean = false;
  categories$!: Observable<ACategoryFromStore[]>;
  categoriesForTaxApplication: number[] = [];
  storeLocations$!: Observable<AStoreLocation[]>;
  allStoreLocations: AStoreLocation[] = [];
  constructor(
    public _FormBuilder: FormBuilder,
    private taxService: TaxesService,
    private notification: NotificationService,
    private policies: ReturnPoliciesService,
    public dialog: MatDialog,
    public router: Router) { 
      this.getTaxes  = this.getTaxes.bind(this);
      this.CloseModal = this.CloseModal.bind(this);
      this.closeApplicationModal = this.closeApplicationModal.bind(this);
    }

    ngOnInit(): void {
      this.Form_AddTax_Load();
      Feather.replace();
      this.getTaxes()
      this.getCategories();
      this.getStoreLocations();
    }

    convertTaxesOnProductToArray(){
      return Object.values(TaxOnProduct).filter(elem => isNaN(parseInt(elem)));
    }

  openDialog() {
    this.dialog.open(PageTipsComponent, {
      panelClass: [
        'customSideBarDialogContainer',
        'animate__animated',
        'animate__slideInRight',
      ],
      width: '470px',
      height: '100vh',
      position: { right: '0' },
      disableClose: true,
    });
  }
  // ADDD() {
  //   this.OpenModal('#exampleModal');
  // }

  Form_AddTax!: FormGroup;
  Form_EditTax!: FormGroup;
  Form_AddTax_Load() {
    this._FormBuilder.group({
      TaxName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern('^[a-zA-Z \-\']+')])],

    })

  }
  Form_EditTax_Process(_FormValue: any) {

  }
  Form_EditTax_Load() {
    this._FormBuilder.group({
      TaxName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern('^[a-zA-Z \-\']+')])],

    })

  }
  Form_AddTax_Process(_FormValue: any) {

  }

  OpenModal(ModalId: any) {
    setTimeout(() => {
      $('#' + ModalId).modal('show');
    }, 100);
  }
  CloseModal(ModalId: any) {
    $('#' + ModalId).modal('hide');
  }
  categories:any = [
    { id: '1', value: 'Food', },
    { id: '1', value: 'Kitchen Appliances', },
    { id: '1', value: 'Jewellery', },
    { id: '1', value: 'Accessories', },
    { id: '1', value: 'Electronics', },
    { id: '1', value: 'Smartphones', },
    { id: '1', value: 'Clothing', },
  ]

  getTaxes(){
    this.taxes$ = this.taxService.getTaxes(1).pipe(map(({data}) => data))
  }

  getCategories(){
    this.categories$ = this.policies.getCategories(1).pipe(map(({data}) => data))
  }

  getStoreLocations(){
  this.storeLocations$ = this.taxService.getStoreLocations('1').pipe(tap(({data}) => this.allStoreLocations = data),map(({data}) => data))
  }

  addToSelectedCat(event: Event){
    const input = event.target as HTMLInputElement;
    if(input.checked){
      this.categoriesForTaxApplication.push(parseInt(input.value));
      return;
    }
    const found = this.categoriesForTaxApplication.indexOf(parseInt(input.value));
    this.categoriesForTaxApplication.splice(found, 1);
  }

  applyTax(event: Event){
    const btn = event.target as HTMLButtonElement;
    const prevText = btn.textContent;
    btn.textContent = 'Applying Policy...';
    this.taxService.applyTaxToCategories({merchantId: 1, categoryIds: this.categoriesForTaxApplication, taxId: this.currentTaxToBeEdit})
    .subscribe({
      next: val => {
        this.notification.modifiedNotificationService({
          icon: 'success',
          title: "Success!!!",
          text: `Tax applied to caategories successfully!!.`,
          didClose: ()=> {this.getTaxes(); this.closeApplicationModal()}
        })
        this.closeModal();
        (document.querySelector('.productModal') as HTMLElement)?.click();
        btn.textContent = prevText;
      },
      error: err => {
        this.notification.modifiedNotificationService({
          icon: 'error',
          title: "Error!!!",
          text: `Unable to apply tax to categories.`,
          didClose: ()=> {this.closeApplicationModal()}
        })
      }
    })
  }

  addTax(event: Event){
    const btn = event.target as HTMLButtonElement;
    const prevText = btn.textContent;
    btn.textContent = 'Saving...';
    btn.disabled = true;
   const {name, taxOnProduct:taxOnProducts, locationsName, enableTax,rate }= this.createATax;
   const form: ATaxToBeCreated = {name,rate,enableTax,storeLocationIds: [parseInt(locationsName)],taxOnProducts,merchantId: 1};
   if(this.edit){
    this.sendEditedTaxToServer({...form, merchantId: 1, taxId: this.currentTaxToBeEdit}, event);
    return;
   }
   this.taxService.createATax(form).subscribe({
    next: val => {
        btn.disabled = false;
        btn.textContent = prevText;
        this.notification.modifiedNotificationService(
          {
        icon: 'success',
        title: "Success!!!",
        text: 'Tax Created Successfully!',
        didClose: () => {this.closeModal();this.getTaxes();}
      })
    },
    error: err => {
      this.notification.modifiedNotificationService(
        {
      icon: 'error',
      title: "Error!!!",
      text: `Tax couldn't be created.`,
      didClose: () => {this.getTaxes(); this.closeModal()}
    })
    }
   })
  }
  closeModal(){
    (document.querySelector('.dismissBtn') as HTMLElement)?.click();
  }

  deleteATax(aTax: ATax){
    this.taxService.deleteTax(aTax.merchantId, aTax.taxId)
    .subscribe({
      next: val => {
        this.notification.modifiedNotificationService(
          {
        icon: 'success',
        title: "Success!!!",
        text: 'Tax deleted Successfully!',
        didClose: () => {this.getTaxes();}
      })
      },
      error: err => {
        this.notification.modifiedNotificationService(
          {
        icon: 'error',
        title: "Error!!!",
        text: 'Unable to delete tax!',
        didClose: () => {this.getTaxes();}
      })
      }
    })
  }

  async editTax(aTax: ATax){
   try {
   const {data} = await lastValueFrom(this.taxService.getATax(aTax.taxId));
   const {taxName:name, rate, taxOnProducts: taxOnProduct, enableTax, id, locationName} = data;
   const foundLocation = this.allStoreLocations.find(elem => (elem.city == locationName[0]) || (elem.state == locationName[0]));
   this.createATax = {name, rate, taxOnProduct, enableTax, locationsName: foundLocation!.id.toString()};
   this.createATax.taxOnProduct = taxOnProduct == TaxOnProduct.Include ? TaxOnProduct.Include : TaxOnProduct.Exclude;
   this.edit = true;
   this.currentTaxToBeEdit = aTax.taxId;
   } catch (error) {
    this.notification.modifiedNotificationService({
      icon: 'error',
      title: "Error!!!",
      text: `Tax couldn't be fetched for update.`,
    })
    this.edit = false;
    this.currentTaxToBeEdit = undefined;
   }
  }

  sendEditedTaxToServer(form: Partial<ATaxToBeCreated> & {merchantId: any, taxId: any, locationNames?: any}, event: Event){
    const btn = event.target as HTMLButtonElement;
    const prevText = btn.textContent;
    btn.textContent = 'Saving...';
    btn.disabled = true;
    form['locationNames'] = form.locationsName; 
   this.taxService.editATax(form)
   .subscribe({
    next: val => {
        btn.disabled = false;
        btn.textContent = prevText;
        this.notification.modifiedNotificationService(
          {
        icon: 'success',
        title: "Success!!!",
        text: 'Tax Updated Successfully!',
        didClose: () => {this.closeModal();this.getTaxes();}
      })
    },
    error: err => {
      this.notification.modifiedNotificationService(
        {
      icon: 'error',
      title: "Error!!!",
      text: `Tax couldn't be updated.`,
      didClose: () => {this.getTaxes(); this.closeModal()}
    })
    }
   })
  }

  trigger(){
    (document.querySelector('.target') as HTMLElement).click();
  }

  closeApplicationModal(){
    (document.querySelector('.closeCategoryModal') as HTMLElement).click()
  }

}
