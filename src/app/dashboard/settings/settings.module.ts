import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { RouterModule, Routes } from '@angular/router';
import { IconsModule, SharedModule } from 'src/app/shared';
import {MatTabsModule} from '@angular/material/tabs';
import { HighchartsChartModule } from 'highcharts-angular';
import { TaxesComponent } from './taxes/taxes.component';
import { DiscountsComponent } from './discounts/discounts.component';
import { ReturnPolicyComponent } from './return-policy/return-policy.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { GeneralSettingsComponent } from './general-settings/general-settings.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { DiscountModalComponent } from './discounts/discount-modal/discount-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { AddDiscountComponent } from './discounts/add-discount/add-discount.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterPipe } from 'src/app/utils/filter.pipe';
import { EditDiscountComponent } from './discounts/edit-discount/edit-discount.component';



const SettingsRoutes:Routes = [
    {
      path:'',
      pathMatch:'full',
      component:TaxesComponent
    },
    {
      path:'general-settings',
      component:GeneralSettingsComponent
    },
    {
      path:'taxes',
      component:TaxesComponent
    },
    {
      path:'discounts',
      component:DiscountsComponent
    },
    {
      path:'add-discount',
      component:AddDiscountComponent
    },
    {
      path:'edit-discount/:discountId',
      component:EditDiscountComponent
    },
    {
      path:'return-policy',
      component:ReturnPolicyComponent
    }
]



@NgModule({
  declarations: [
    GeneralSettingsComponent,DiscountsComponent, DiscountModalComponent, AddDiscountComponent,
    FilterPipe, TaxesComponent, ReturnPolicyComponent,
    EditDiscountComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(SettingsRoutes),
    IconsModule,
    SharedModule,
    MatCheckboxModule,
    NgxPaginationModule,
    MatSlideToggleModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers:[
    DatePipe
  ]
})
export class SettingsModule { }
