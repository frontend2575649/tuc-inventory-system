import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import { ProductCatalogComponent } from "./products-catalog.component";
import { FeatherModule } from 'angular-feather';
import { NgSelectModule } from '@ng-select/ng-select';
import { IconsModule, SharedModule } from 'src/app/shared';
const routes: Routes = [
  {
      path: "",
      component: ProductCatalogComponent,

  }
];
@NgModule({
  declarations: [ProductCatalogComponent],
  imports: [
    CommonModule,SharedModule,
    FeatherModule,IconsModule,
    NgSelectModule,
    RouterModule.forChild(routes),
  ]
})
export class ProductsCatalogModule { }
