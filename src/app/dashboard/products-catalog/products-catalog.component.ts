import { Component, OnInit } from '@angular/core';
import * as Feather from "feather-icons";
import { NgSelectComponent } from '@ng-select/ng-select';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { NotificationService } from '../../services/notification.service';
import { CashierService } from 'src/app/services/cashier.service';
import { Store } from 'src/app/interface/interfaces';
@Component({
  selector: 'app-products-catalog',
  templateUrl: './products-catalog.component.html',
  styleUrls: ['./products-catalog.component.css']
})
export class ProductCatalogComponent implements OnInit {
  ProductData: any;
  storeList:Store[]=[];
  constructor(private notify: NotificationService, private cashierService: CashierService, private _Router: Router,  public _apiService: ApiService, public _notification: NotificationService,) { }

  ngOnInit(): void {
    Feather.replace();
    this.getProducts();
    this.getStores()
  }

  getStores(){
    this.cashierService.getStoreList('1').subscribe(response => {
      let res = response
      if( response.responseCode == 200 ){
        this.storeList = res.data
      }
    },(error) => {
        this.notify.NotifyError(error.error.message);
      },() => {} )
  }

  getProducts() {
    this._apiService.getData('product/getProducts',).subscribe((Response: any) => {
      if (Response && Response.responseCode == "200") {
       this.ProductData=Response.data ;
      }
    },
      _Error => {
        this._notification.HandleException(_Error);
      });
  }

  GotoAddProduct() {
    this._Router.navigate(['/add-product']);
  }

}
