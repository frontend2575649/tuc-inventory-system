import { Component } from '@angular/core';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss']
})
export class OnboardingComponent {
  page:number = 1;
  selected = 'option2'; 
  subPage:string = '';
  isMainPage:boolean = true;
  constructor()
  {

  }

  PerformAction(data:number){
    switch(data){
      case 1:
        this.isMainPage= false
        this.subPage = 'taxSettings';
      break
      case 2:
        this.isMainPage = false
        this.subPage = 'business'
      break
      case 3:
        this.isMainPage = false
        this.subPage = 'policy'
      break
      case 4:
        this.isMainPage =  false
        this.subPage = 'product'
      break
    }
  }
  resetPage(){
    this.isMainPage;
    this.subPage = ''
  }

  saveData(){
   
  }
  
}
