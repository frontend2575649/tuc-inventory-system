import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { IconsModule, SharedModule } from 'src/app/shared';
import { OnboardingComponent } from './onboarding.component';

import {MatSelectModule} from '@angular/material/select';


const ViewRoutes: Routes = [
      {
        path:'',
        component:OnboardingComponent
      }
]

@NgModule({
  declarations: [
    OnboardingComponent], 
    imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ViewRoutes),
    IconsModule,
    MatSelectModule
  ]
})
export class OnboardingModule { }
