import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

interface ModalData {

}
@Component({
  selector: 'app-invoice-slip',
  templateUrl: './invoice-slip.component.html',
  styleUrls: ['./invoice-slip.component.scss']
})

export class InvoiceSlipComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<InvoiceSlipComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ModalData)
  { }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

}
