import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import * as Highcharts from 'highcharts';
import { Invoice, Terminal, TestData } from 'src/app/interface/interfaces';
import { InvoiceSlipComponent } from '../invoice-slip/invoice-slip.component';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CashierService } from 'src/app/services/cashier.service';
import { NotificationService } from 'src/app/utils/notification.service';

@Component({
  selector: 'app-cashier-details',
  templateUrl: './cashier-details.component.html',
  styleUrls: ['./cashier-details.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class CashierDetailsComponent implements OnInit {
  @ViewChild('chart', { static: true })
  chartEl!: ElementRef;
  isInvoice: boolean = true
  name: string = ""
  color: string = ""

  @ViewChild(InvoiceSlipComponent) invoiceSlip!: InvoiceSlipComponent;

  data:any;

  userId!: string;
  private sub: any;
  cashier: any;
  invoiceList: Invoice[] = [];
  cashierId: any;
  overview: any;
  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: any
  posList: Terminal[] = [];
  cashierCode: any;

  constructor(private route: ActivatedRoute, private cashierService: CashierService, private notify: NotificationService,
    private router: Router, public dialog: MatDialog) {
      this.sub = this.route.params.subscribe(params => {
        this.userId = params['userId'];
      });
    }

    ngOnInit() {
      this.getCashierById(this.userId)
    }

  getInvoiceCounts(id: number){
    this.getChart()
    this.cashierService.getCashierOverview(id).subscribe(response => {
      if( response.responseCode == 200 ){
        this.overview = response.data
      } else {
        this.notify.showError(response.responseMessage);

      } },(error) => {
        this.notify.showError(error.error.message);
      },() => {} )
    }

    getChart(){
      this.chartOptions = {
        chart:{
          type:'donut'
        },
        title:{
          text:''
        },
        series:[{
          name:'Amount',
          type:'pie',
          innerSize: '60%',
          // data: [this.overview.totalCardsAmount,this.overview.totalCashAmount]
          data: [{name: 'Card',y:200}, {name: 'Cash',y:1200}]
        }],
        credits: {
          enabled: false
        }
      };
    }

    getCashierById(userId: string){
      this.cashierService.getCashierByUserId(userId).subscribe(response => {
        console.log(response.data)
        if( response.responseCode == 200 ){
          this.cashier = response.data
          this.cashierId = this.cashier.id
          this.cashierCode = this.cashier.cashierCode
          this.getInvoiceCounts(this.cashierId)
          this.getInvoiceByCashier(this.cashierCode)
          this.getPosByCashier(this.cashierId)
      } else {
        this.notify.showError(response.responseMessage);

      } },(error) => {
        this.notify.showError(error.error.message);
      },() => {} )
  }

  getInvoiceByCashier(userId: string){
    this.cashierService.getInvoiceByCashier(userId).subscribe(response => {
      if( response.responseCode == 200 ){
        this.invoiceList = response.data
      } else {
        this.notify.showError(response.responseMessage);
      } },(error) => {
        this.notify.showError(error.error.message);
      },() => {} )
  }

  getPosByCashier(id: number){
    this.cashierService.getPosList(id).subscribe(response => {
      if( response.responseCode == 200 ){
        this.posList = response.data.terminals
      } else {
        this.notify.showError(response.responseMessage);
      } },(error) => {
        this.notify.showError(error.error.message);
      },() => {} )
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    console.log('tabChangeEvent => ', tabChangeEvent);
    console.log('index => ', tabChangeEvent.index);
    if(tabChangeEvent.index == 1){
      this.isInvoice = false
    }else{
      this.isInvoice = true
    }
  }

  chartCallback: Highcharts.ChartCallbackFunction = function (chart): void {
    setTimeout(() => {
        chart.reflow();
    },0);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(InvoiceSlipComponent, {
      width: '450px',
      data: { name: this.name, color: this.color }
    });
    dialogRef.afterClosed().subscribe(res => {
      this.color = res;
    });

  }

}
