import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store, TestData } from 'src/app/interface/interfaces';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { InvoiceSlipComponent } from '../invoice-slip/invoice-slip.component';
import { CashierService } from 'src/app/services/cashier.service';
import { NotificationService } from 'src/app/utils/notification.service';


@Component({
  selector: 'app-cashier-home',
  templateUrl: './cashier-home.component.html',
  styleUrls: ['./cashier-home.component.scss']
})
export class CashierHomeComponent implements OnInit {
  isDetailsPage: boolean = false
  storeForm = new FormGroup({
    //stores: ['', Validators.required]
  })

  data:any;
  tableData: TestData[] = [
    {
      ReferenceId: 13254,
      ReferenceKey: "bf18878177ab49e9ba7fbc36dc6a6657",
      DisplayName: "ThankUCashBs",
      ContactNumber: "2347056635295",
      EmailAddress: "paul-ominyi+7@thankucash.com",
      Address: "1 Wole Olanipekun Cl, Lekki Phase I 106104, Lekki, Lagos, Nigeria",
      MerchantId: 13253,
      MerchantKey: "b25a91f50bbe4a7686116d2a13b47e55",
      CreateDate: "2023-01-06T13:41:55",
      StatusId: 2,
      StatusCode: "default.active",
      StatusName: "Active",
      Categories: [
        {
          CategoryId: 10,
          CategoryKey: "10",
          CategoryName: "Groceries"
        }
      ]
    },
    {
      ReferenceId: 1325962,
      ReferenceKey: "010058ffae064702bd9820f6f0abb255",
      DisplayName: "New store",
      ContactNumber: "2348098998989",
      EmailAddress: "paul-ominyi+7@thankucash.com",
      Address: "131 Obafemi Awolowo Way, Oba Akran 100271, Ikeja, Lagos, Nigeria",
      MerchantId: 13253,
      MerchantKey: "b25a91f50bbe4a7686116d2a13b47e55",
      CreateDate: "2023-01-25T13:08:18",
      StatusId: 2,
      StatusCode: "default.active",
      StatusName: "Active",
      Categories: [
        {
          CategoryId: 7,
          CategoryKey: "7",
          CategoryName: "Fashion"
        }
      ]
    },
    {
      ReferenceId: 1319110,
      ReferenceKey: "180ffde1d79f4c40a23849da80d3005e",
      DisplayName: "Deal  iPhone Store",
      ContactNumber: "2348098779259",
      EmailAddress: "paul-ominyi+7@thankucash.com",
      Address: "H8VP+JG5, Ikeja 101233, Ikeja, Lagos, Nigeria",
      MerchantId: 13253,
      MerchantKey: "b25a91f50bbe4a7686116d2a13b47e55",
      CreateDate: "2023-01-19T10:45:50",
      StatusId: 2,
      StatusCode: "default.active",
      StatusName: "Active",
      Categories: [
        {
          CategoryId: 222,
          CategoryKey: "58861be927c642f9bcf55d64f02efbc7",
          CategoryName: "iphone"
        }
      ]
    }
  ]
  cashierList: any;
  allCashier: boolean = false
  activeCashier: boolean = false
  inActiveCashier: boolean = false
  blockedCashier: boolean = false
  cashierCount!: { totalActive: number; totalInActive: number; totalIsBlocked: number; };
  storeList: Store[] = [];

  constructor(private cashierService: CashierService, private notify: NotificationService,
            private fb: FormBuilder, public dialog: MatDialog
    ) { }

  ngOnInit() {
    this.getCashiersCount()
    this.getCashiers('')
    this.getStores()
  }

  getStores(){
    this.cashierService.getStoreList('1').subscribe(response => {
      let res = response
      if( response.responseCode == 200 ){
        this.storeList = res.data
      } else {
        this.notify.showError(response.responseMessage);

      } },(error) => {
        this.notify.showError(error.error.message);
      },() => {} )
  }

  updatestate(status: string){
    if(status == 'Active'){
      this.activeCashier = true
      this.allCashier = false
      this.blockedCashier = false
      this.inActiveCashier = false
    }else if(status == 'InActive'){
      this.activeCashier = false
      this.allCashier = false
      this.blockedCashier = false
      this.inActiveCashier = true
    }else if(status == 'Blocked'){
      this.activeCashier = false
      this.allCashier = false
      this.blockedCashier = true
      this.inActiveCashier = false
    }else{
      this.activeCashier = false
      this.allCashier = true
      this.blockedCashier = false
      this.inActiveCashier = false
    }

  }

  getCashiersCount(){
    this.cashierService.getCashierCounts().subscribe(response => {
      let res = response
      if( response.responseCode == 200 ){
        this.cashierCount = res.data as any
      } else {
        this.notify.showError(response.responseMessage);
      } },(error) => {
        this.notify.showError(error.error.message);
      },() => {} )
  }

  getCashiers(status: string){
    this.updatestate(status)
    let req = {
      dateFrom: "2022-05-18T20:31:06.121Z",
      dateTo: "2023-05-28T20:31:06.121Z",
      searchKey: "",
      status: status,
      sortKey: ""
    }
    this.cashierService.getCashierList(req).subscribe(response => {
      let res = response
      if( response.responseCode == 200 ){
        this.cashierList = res.data
        this.cashierList.sort(function(a:any, b:any) {
          var c = new Date(a.dateCreated);
          var d = new Date(b.dateCreated);
          return Number(c) - Number(d);
      });
      this.cashierList.reverse()
      console.log(this.cashierList)
      } else {
        this.notify.showError(response.responseMessage);

      } },(error) => {
        this.notify.showError(error.error.message);
      },() => {} )
  }

}
