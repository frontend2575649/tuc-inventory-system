import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashierDetailsComponent } from './cashier-details/cashier-details.component';
import { CashierHomeComponent } from './cashier-home/cashier-home.component';
import { InvoiceSlipComponent } from './invoice-slip/invoice-slip.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IconsModule, SharedModule } from 'src/app/shared';
import { MatTabsModule } from '@angular/material/tabs';
import { HighchartsChartModule } from 'highcharts-angular';
import {MatDialogModule} from '@angular/material/dialog';

const routes: Routes = [
  { path: '', component:CashierHomeComponent},
  {path: 'cashier-details/:userId', component:CashierDetailsComponent}
];

@NgModule({
  declarations: [
    CashierDetailsComponent,
    CashierHomeComponent,
    InvoiceSlipComponent
  ],
  imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      RouterModule.forChild(routes),
      IconsModule,
    SharedModule,
    MatTabsModule,
    MatDialogModule,
    HighchartsChartModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [InvoiceSlipComponent]
})
export class CashiersModule { }
