import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import { LooseCatalogComponent } from "./loose-product.component";
import { FeatherModule } from 'angular-feather';
import { NgSelectModule } from '@ng-select/ng-select';
import { IconsModule, SharedModule } from 'src/app/shared';
const routes: Routes = [
  {
      path: "",
      component: LooseCatalogComponent,

  }
];
@NgModule({
  declarations: [LooseCatalogComponent],
  imports: [
    CommonModule,SharedModule,
    FeatherModule,IconsModule,
    NgSelectModule,
    RouterModule.forChild(routes),
  ]
})
export class LooseCatalogModule { }
