import { Component, OnInit } from '@angular/core';
import * as Feather from "feather-icons";
import { NgSelectComponent } from '@ng-select/ng-select';
import { Router } from '@angular/router';
import { CashierService } from 'src/app/services/cashier.service';
import { Store } from 'src/app/interface/interfaces';
import { NotificationService } from 'src/app/utils/notification.service';
@Component({
  selector: 'app-loose-product',
  templateUrl: './loose-product.component.html',
  styleUrls: ['./loose-product.component.css']
})
export class LooseCatalogComponent implements OnInit {
  storeList: Store[] = [];

  constructor(private notify: NotificationService, private cashierService: CashierService, private _Router: Router,) { }

  ngOnInit(): void {
    Feather.replace();
    this.getStores()
  }

  getStores(){
    this.cashierService.getStoreList('1').subscribe(response => {
      let res = response
      if( response.responseCode == 200 ){
        this.storeList = res.data
      }
    },(error) => {
        this.notify.showError(error.error.message);
      },() => {} )
  }
  GotoAddProduct() {
    this._Router.navigate(['/add-product']);
  }


}
