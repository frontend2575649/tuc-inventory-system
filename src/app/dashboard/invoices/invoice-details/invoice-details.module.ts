import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceDetailsComponent } from './invoice-details.component';
import { RouterModule, Routes } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';
import { FeatherModule } from 'angular-feather';
import { allIcons } from 'angular-feather/icons';
import { IconsModule, SharedModule } from 'src/app/shared';


@NgModule({
  declarations: [InvoiceDetailsComponent],
  imports: [
    CommonModule,
    NgSelectModule,
    NgxPaginationModule,
    FeatherModule.pick(allIcons),
    IconsModule,
    SharedModule,
  ],
  exports: [
    FeatherModule
  ]
})
export class InvoiceDetailsModule { }
