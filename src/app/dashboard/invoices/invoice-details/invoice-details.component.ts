import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import * as Feather from 'feather-icons';

interface ModalData {

}
@Component({
    selector: 'app-invoice-details',
    templateUrl: './invoice-details.component.html',
    styleUrls: ['./invoice-details.component.scss']
})

export class InvoiceDetailsComponent implements OnInit {
    constructor(
        public dialogRef: MatDialogRef<InvoiceDetailsComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ModalData) { }

    onClose(): void {
        this.dialogRef.close();
    }

    ngOnInit() {
        Feather.replace();
     }

}
