import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoicesComponent } from './invoices.component';
import { RouterModule ,Routes } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';

import { FeatherModule } from 'angular-feather';
import { allIcons } from 'angular-feather/icons';

const DealRoutes:Routes = [
  {
    path: '',
    component: InvoicesComponent
  }
]

@NgModule({
  declarations: [InvoicesComponent],
  imports: [
    CommonModule,
    NgSelectModule,
    RouterModule.forChild(DealRoutes),
    NgxPaginationModule,
    FeatherModule.pick(allIcons)
  ],
  exports:[
    FeatherModule
  ]
})
export class InvoicesModule { }
