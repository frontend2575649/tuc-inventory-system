import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
declare var $: any;
declare var moment: any;
import * as Feather from 'feather-icons';
import { NgSelectComponent } from '@ng-select/ng-select';
import { InvoiceDetailsComponent } from './invoice-details/invoice-details.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';



@Component({
    selector: 'app-invoices',
    templateUrl: './invoices.component.html',
    styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {

    @ViewChild('ngSelectComponent') ngSelectComponent!: NgSelectComponent;

    name: string = ""
    color: string = ""
    @ViewChild(InvoiceDetailsComponent) invoiceSlip!: InvoiceDetailsComponent;

    ngOnInit(): void {
        Feather.replace();
    }
    constructor(private fb: FormBuilder, public dialog: MatDialog
    ) { }

    OpenDetailsModal() {
        const dialogRef = this.dialog.open(InvoiceDetailsComponent, {
            panelClass: [
                'customSideBarDialogContainer',
                'animate__animated',
                'animate__slideInRight',
            ],
            width: '450px',
            height: '100vh',
            position: { right: '0' },
            disableClose: true,
            data: { name: this.name, color: this.color }
        });
        dialogRef.afterClosed().subscribe(res => {
            this.color = res;
        });

    }



}