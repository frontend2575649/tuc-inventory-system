import { Component, OnInit } from '@angular/core';
import * as Feather from 'feather-icons';
import * as Highcharts from 'highcharts';
import { ApiService } from 'src/app/services/api.service';
// import { NotificationService } from 'src/app/services/notification.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pos-overview',
  templateUrl: './pos-overview.component.html',
  styleUrls: ['./pos-overview.component.scss']
})

export class PosOverviewComponent implements OnInit {
  ngOnInit(): void {
    Feather.replace();
    this.getTerminal();
  }
  Highcharts: typeof Highcharts = Highcharts;
  constructor(
    public _FormBuilder: FormBuilder,
    public dialog: MatDialog,
    public router: Router,
    public _apiService: ApiService,
    // public _notifyService: NotificationService
    ) { }


  chartOptions: Highcharts.Options = {
    chart: {
      type: 'column',
      height: 300
    },
    title: {
      text: 'POS Overview'
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: -45,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif'
        }
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Population (millions)'
      }
    },

    tooltip: {
      pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>'
    },
    credits: {
      enabled: false
    },
    series: [{
      type: 'column',
      data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
    }
    ]
  };


  public TerminalData: any = {
    ContactNumber:null,
    EmailId:null,
    id: null,
    serialNumber: null,
    terminalID: null,
    lastTransactionDate: null,
    merchantId: null,
    merchantName: null,
    storeId: null,
    storeName: null,
    storeLocation: null,
    type: null,
    issuerBankId: null,
    issuerBank: null,
    issuerBankIconUrl: null,
    providerId: null,
    provider: null,
    dateCreated: null,
  };
  terminalId!: number;

  getTerminal() {
    this.terminalId = 1;
    this._apiService.getData('pos/getTerminal?terminalId=' + this.terminalId)
      .subscribe((data: any) => {
        this.TerminalData = data.data;
        console.log(this.TerminalData);
      });
  }

}
