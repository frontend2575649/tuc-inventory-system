import { Component, OnInit } from '@angular/core';
import { PageTipsComponent } from './page-tips/page-tips.component';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ApiService } from 'src/app/services/api.service';
import { NotificationService } from 'src/app/services/notification.service';


@Component({
  selector: 'app-pos-details',
  templateUrl: './pos-details.component.html',
  // styleUrls: ['./pos-details.component.scss']
})

export class PosDetailsComponent implements OnInit {

  ngOnInit(): void {
    Feather.replace();
    this.getTerminals();
  }
  constructor(
    public _FormBuilder: FormBuilder,
    public dialog: MatDialog,
    public router: Router,
    public _apiService: ApiService,
    public _notifyService: NotificationService) { }

  openDialog() {
    this.dialog.open(PageTipsComponent, {
      panelClass: [
        'customSideBarDialogContainer',
        'animate__animated',
        'animate__slideInRight',
      ],
      width: '470px',
      height: '100vh',
      position: { right: '0' },
      disableClose: true,
    });
  }

  TerminalsData: any;
  TerminalStatus: any = {};
  AccountId!: number;

  getTerminals() {
    this.AccountId = 1;
    this._apiService.getData('pos/getTerminals?merchantId=' + this.AccountId)
      .subscribe((data: any) => {
        this.TerminalStatus = data.data;
        this.TerminalsData = data.data.terminals;
      });
  }


}
