import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PosDetailsComponent } from './pos-details.component';
import { PageTipsComponent } from './page-tips/page-tips.component';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule, Routes } from '@angular/router';
import { IconsModule } from 'src/app/shared/icons.module';
import { SharedModule } from 'src/app/shared';
import {MatMenuModule} from '@angular/material/menu';
import { NgxPaginationModule } from 'ngx-pagination';
import { PosOverviewComponent } from './pos-overview/pos-overview.component';
import { HighchartsChartModule } from 'highcharts-angular';

import {MatTabsModule} from '@angular/material/tabs';

const routes: Routes = [
  {
    path: '',
    component: PosDetailsComponent,
  },
  {
    path:'pos-overview',
    component:PosOverviewComponent
  }
  
];

@NgModule({
  declarations: [
    PageTipsComponent,
    PosDetailsComponent,
    PosOverviewComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    NgxPaginationModule,
    IconsModule,
    HighchartsChartModule,
    MatTabsModule,
    SharedModule,
    MatMenuModule,
    RouterModule.forChild(routes),
  ],
})
export class PosDetailsModule {}
