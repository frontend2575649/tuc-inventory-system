import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PosRoutingModule } from './pos-routing.module';

import { MatDialogModule } from '@angular/material/dialog';
import { IconsModule } from 'src/app/shared/icons.module';
import { SharedModule } from 'src/app/shared';

@NgModule({
  declarations: [
  
  ],
  imports: [
    CommonModule,
    PosRoutingModule,
    MatDialogModule,
    IconsModule,
    SharedModule,
  ]
})
export class PosModule { }


