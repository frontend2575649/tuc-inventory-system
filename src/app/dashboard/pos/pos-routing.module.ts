import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PosDetailsComponent } from './pos-details/pos-details.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'pos-details',
    pathMatch: 'full',
  },
  {
    path: 'pos-details',
    loadChildren: () =>
      import('./pos-details/pos-details.module').then(
        (m) => m.PosDetailsModule
      ),
  },


  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PosRoutingModule {}
