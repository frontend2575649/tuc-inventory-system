import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { IconsModule, SharedModule } from 'src/app/shared';
import { CategoriesComponent } from './categories/categories.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NewCategoryComponent } from './new-category/new-category.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


const routes: Routes = [
  {
    path: '',
    component: CategoriesComponent,
  },
];

@NgModule({
  declarations: [CategoriesComponent, NewCategoryComponent],
  imports: [
    
    CommonModule,
    SharedModule,
    IconsModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatChipsModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
})
export class CategoriesModule {}
