import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject,
} from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { CategoriesService } from 'src/app/services/categories.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from 'src/app/utils/notification.service';
//import { NotificationService } from 'src/app/services/notification.service';
export interface Options {
  name: string;
}

@Component({
  selector: 'app-new-category',
  templateUrl: './new-category.component.html',
  styleUrls: ['./new-category.component.scss'],
})
export class NewCategoryComponent implements OnInit {
  panelOpenState = false;
  optionList: string[] = [];
  url: any;
  subCategoriesData: any;
  merchantId: any;
  names: any;
  manageCategoryForm!: FormGroup;

  @ViewChild('subCategoryInput')
  subCategoryInput!: ElementRef<HTMLInputElement>;

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;

  constructor(
    private categoryService: CategoriesService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<NewCategoryComponent>,
    private notify: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.getSubCategories();

    this.names = this.subCategoriesData?.map(function (subcategoryNames: any) {
      return subcategoryNames.name;
    });
    console.log(this.names, 'names');

    this.buildForm();
  }

  buildForm() {
    this.manageCategoryForm = this.fb.group({
      icon: [''],
      categoryName: ['', Validators.required],
      subCategoryId: [],
    });
  }

  get manageCategoryFormControls() {
    return this.manageCategoryForm.controls;
  }

  getErrorMessage(instance: string) {
    if (
      instance === 'icon' &&
      this.manageCategoryFormControls['icon'].hasError('required')
    ) {
      return `Please choose a file`;
    } else if (
      instance === 'categoryName' &&
      this.manageCategoryFormControls[' categoryName'].hasError('required')
    ) {
      return `Please enter category name`;
    } else {
      return;
    }
  }
  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    if (value) {
      this.optionList.push(value);
    }

    event.chipInput!.clear();
  }

  remove(fruit: string): void {
    const index = this.optionList.indexOf(fruit);

    if (index >= 0) {
      this.optionList.splice(index, 1);
    }
  }

  onSelectFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => {
        // called once readAsDataURL is completed
        this.url = event.target?.result;
      };
    }
  }

  getSubCategories() {
    this.merchantId = 1;
    this.categoryService
      .getSubCategories(this.merchantId)
      .subscribe((resData) => {
        if (resData.responseCode == 200) {
          this.subCategoriesData = resData.data;

          console.log('hjfhhfh', this.subCategoriesData);
        }
      });
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.optionList.push(event.option.viewValue);
    this.subCategoryInput.nativeElement.value = '';
    //this.fruitCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.subCategoriesData.filter((fruit: any) =>
      fruit.toLowerCase().includes(filterValue)
    );
  }

  patchCategoryForm() {
    if (this.data?.instance === 'update') {
      this.manageCategoryForm.patchValue({
        icon: this.data?.categories?.icon,
        categoryName: this.data?.categories?.categoryName,
        subCategoryId: this.data?.categories?.subCategoryId,
        
      });
    }
  }

  onSubmit() {
    if (this.data?.instance === 'create') {
      this.createCategories();
    } else if (this.data?.instance === 'update') {
      this.updateCategories();
    }
  }




  updateCategories() {
    let body = {
      merchantId: 1,
      categoryId: 2,
      name: this.manageCategoryForm.value.categoryName,
      subCategoryIds: this.manageCategoryForm.value.subCategoryId,
    };
    this.categoryService
      .postData('category/updateCategory', body)
      .subscribe((resData) => {
        if (resData) {
          this.notify.showSuccess(resData.responseMessage);
        } else {
          this.notify.showError(resData.responseMessage);
        }
      });
  }

  createCategories() {
    let body = {
      merchantId: 1,
      name: this.manageCategoryForm.value.categoryName,
      subCategoryIds: this.manageCategoryForm.value.subCategoryId,
    };
    this.categoryService
      .postData('category/createCategory', body)
      .subscribe((resData) => {
        if (resData) {
          this.notify.showSuccess(resData.responseMessage);
        } else {
          this.notify.showError(resData.responseMessage);
        }
      });
  }
}
