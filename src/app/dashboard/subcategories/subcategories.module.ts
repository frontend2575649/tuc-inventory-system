import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubcategoriesComponent } from './subcategories.component';
import { RouterModule, Routes } from '@angular/router';
import { IconsModule, SharedModule } from 'src/app/shared';
import { NewSubcategoriesComponent } from './new-subcategories/new-subcategories.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: SubcategoriesComponent,
  },
];



@NgModule({
  declarations: [
    SubcategoriesComponent,
    NewSubcategoriesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IconsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ]
})
export class SubcategoriesModule { }
