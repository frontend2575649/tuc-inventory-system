import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CategoriesService } from 'src/app/services/categories.service';
import { NewSubcategoriesComponent } from './new-subcategories/new-subcategories.component';

@Component({
  selector: 'app-subcategories',
  templateUrl: './subcategories.component.html',
  styleUrls: ['./subcategories.component.scss'],
})
export class SubcategoriesComponent implements OnInit {
  merchantId: any;
  subCategoriesData: any;

  constructor(
    public dialog: MatDialog,
    private categoryService: CategoriesService
  ) {}

  ngOnInit(): void {
    this.getSubCategories();
  }

  openDialog(instance: 'create' | 'update', subcategoriesData?: any) {
    this.dialog.open(NewSubcategoriesComponent, {
      data: {
        instance: instance,
        categories: subcategoriesData,
      },
    });
  }

  getSubCategories() {
    this.merchantId = 1;
    this.categoryService.getSubCategories(this.merchantId).subscribe((resData) => {
      if (resData.responseCode == 200) {
        this.subCategoriesData = resData.data;

        console.log ('hjfhhfh', this.subCategoriesData)
      }
    });
  }
}
