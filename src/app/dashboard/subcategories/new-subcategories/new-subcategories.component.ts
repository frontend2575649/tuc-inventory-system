import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Inject,
} from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { CategoriesService } from 'src/app/services/categories.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from 'src/app/utils/notification.service';
//

@Component({
  selector: 'app-new-subcategories',
  templateUrl: './new-subcategories.component.html',
  styleUrls: ['./new-subcategories.component.scss'],
})
export class NewSubcategoriesComponent implements OnInit {
  merchantId: any;

  manageCategoryForm!: FormGroup;

  constructor(
    private categoryService: CategoriesService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<NewSubcategoriesComponent>,
    private notify: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.buildForm();
    console.log(this.data, "dataa")

    this.patchSubCategories();
  }

  buildForm() {
    this.manageCategoryForm = this.fb.group({
      name: ['', Validators.required],
    });
  }

  get manageCategoryFormControls() {
    return this.manageCategoryForm.controls;
  }

  getErrorMessage(instance: string) {
    if (
      instance === 'name' &&
      this.manageCategoryFormControls['name'].hasError('required')
    ) {
      return `Please enter subcategory name`;
    } else {
      return;
    }
  }

  patchSubCategories() {
    if (this.data?.instance === 'update') {
      this.manageCategoryForm.patchValue({
        name: this.data?.categories?.name,
        
      });
    }
  }

  onSubmit() {
    if (this.data?.instance === 'create') {
      this.createCategories();
    } else if (this.data?.instance === 'update') {
      this.updateCategories();
    }
  }

  updateCategories() {
    let body = {
      merchantId: 1,
      name: this.manageCategoryForm.value.name,
      categoryId: this.data?.categories?.id,
    };
    this.categoryService
      .putData('subCagetory/updateCategory', body)
      .subscribe((resData) => {
        if (resData) {
          this.notify.showSuccess(resData.responseMessage);
        } else {
          this.notify.showError(resData.responseMessage);
        }
      });
  }

  createCategories() {
    let body = {
      merchantId: 1, 
      name: this.manageCategoryForm.value.name,
      
    };
    this.categoryService
      .postData('subCagetory/createCategory', body)
      .subscribe((resData) => {
        if (resData) {
          this.notify.showSuccess(resData.responseMessage);
        } else {
          this.notify.showError(resData.responseMessage);
        }
      });
  }
}
