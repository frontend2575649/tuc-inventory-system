import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelpComponent } from './help.component';
import { RouterModule ,Routes } from '@angular/router';
import { IconsModule, SharedModule } from 'src/app/shared';
import { PopularQuestionComponent } from './popular-question/popular-question.component';


const routes:Routes = [
  {
    path: '',
    component: HelpComponent
  },
  {
    path: 'popular-question',
    component: PopularQuestionComponent
  }
]

@NgModule({
  declarations: [
    HelpComponent,
    PopularQuestionComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IconsModule,
    RouterModule.forChild(routes)
  ]
})
export class HelpModule { }
