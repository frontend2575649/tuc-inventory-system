import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReturnRoutingModule } from './return-routing.module';
import { MatDialogModule } from '@angular/material/dialog';
import { IconsModule } from 'src/app/shared/icons.module';
import { SharedModule } from 'src/app/shared';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReturnRoutingModule,
    MatDialogModule,
    IconsModule,
    SharedModule,
  ],
})
export class ReturnModule {}
