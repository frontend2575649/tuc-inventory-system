import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PosSalesReturnComponent } from './pos-sales-return.component';
import { RouterModule, Routes } from '@angular/router';
import { IconsModule, SharedModule } from 'src/app/shared';
import {MatTabsModule} from '@angular/material/tabs';
import { DetailsPosOrderComponent } from './details-pos-order/details-pos-order.component';
import { InvoicesComponent } from './invoices/invoices.component';



const routes: Routes = [
  {
    path: '',
    component: PosSalesReturnComponent,
  },

  {
    path: 'details-pos-return',
    component: DetailsPosOrderComponent,
  },
  
];

@NgModule({
  declarations: [
    PosSalesReturnComponent,
    DetailsPosOrderComponent,
    InvoicesComponent
  ],
  imports: [
    CommonModule,
    IconsModule,
    MatTabsModule,
    SharedModule,
    RouterModule.forChild(routes),
  ]
})
export class PosSalesReturnModule { }
