import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details-pos-order',
  templateUrl: './details-pos-order.component.html',
  styleUrls: ['./details-pos-order.component.scss']
})
export class DetailsPosOrderComponent {

  constructor (
    public router: Router,
    private route: ActivatedRoute,
  ) {
    
  }
  back() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }
}
