import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosSalesReturnComponent } from './pos-sales-return.component';

describe('PosSalesReturnComponent', () => {
  let component: PosSalesReturnComponent;
  let fixture: ComponentFixture<PosSalesReturnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosSalesReturnComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PosSalesReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
