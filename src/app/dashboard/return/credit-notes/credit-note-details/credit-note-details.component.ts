import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReturnsService } from 'src/app/services/returns.service';


@Component({
  selector: 'app-credit-note-details',
  templateUrl: './credit-note-details.component.html',
  styleUrls: ['./credit-note-details.component.scss'],
})
export class CreditNoteDetailsComponent implements OnInit {
  creditNoteId: any;
  creditNotesDetails: any;
  
  constructor(
    private router: Router,
  private route: ActivatedRoute,
  private returnsService: ReturnsService
  ) {
 
  }

  ngOnInit(): void {
    this.getCreditNoteDetails();
  }
  back() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }

  getCreditNoteDetails() {
    this.creditNoteId = 12;
    this.returnsService
      .getCreditNotesDetails(this.creditNoteId)
      .subscribe((resData) => {
        if (resData.statusCode == 200) {
          this.creditNotesDetails = resData.data;
          
        }
      });
  }
}
