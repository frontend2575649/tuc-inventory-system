import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditNotesComponent } from './credit-notes.component';
import { PageTipsComponent } from './page-tips/page-tips.component';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule, Routes } from '@angular/router';
import { IconsModule } from 'src/app/shared/icons.module';
import { SharedModule } from 'src/app/shared';
import { CreditNoteDetailsComponent } from './credit-note-details/credit-note-details.component';
import {MatMenuModule} from '@angular/material/menu';

const routes: Routes = [
  {
    path: '',
    component: CreditNotesComponent,
  },
  {
    path: 'credit-note-details',
    component: CreditNoteDetailsComponent,
  },
];

@NgModule({
  declarations: [
    PageTipsComponent,
    CreditNotesComponent,
    CreditNoteDetailsComponent,
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    IconsModule,
    SharedModule,
    MatMenuModule,
    RouterModule.forChild(routes),
  ],
})
export class CreditNotesModule {}
