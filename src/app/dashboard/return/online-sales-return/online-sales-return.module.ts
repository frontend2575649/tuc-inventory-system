import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnlineSalesReturnComponent } from './online-sales-return.component';
import { RouterModule, Routes } from '@angular/router';
import { IconsModule, SharedModule } from 'src/app/shared';
import {MatTabsModule} from '@angular/material/tabs';

const routes: Routes = [
  {
    path: '',
    component: OnlineSalesReturnComponent,
  },
  
];



@NgModule({
  declarations: [
    OnlineSalesReturnComponent
  ],
  imports: [
    CommonModule,
    IconsModule,
    SharedModule,
    MatTabsModule,
    RouterModule.forChild(routes),
  ]
})
export class OnlineSalesReturnModule { }
