import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineSalesReturnComponent } from './online-sales-return.component';

describe('OnlineSalesReturnComponent', () => {
  let component: OnlineSalesReturnComponent;
  let fixture: ComponentFixture<OnlineSalesReturnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnlineSalesReturnComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OnlineSalesReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
