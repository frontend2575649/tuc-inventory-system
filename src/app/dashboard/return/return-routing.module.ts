import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreditNotesComponent } from './credit-notes/credit-notes.component';
import { OnlineSalesReturnComponent } from './online-sales-return/online-sales-return.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'credit-notes',
    pathMatch: 'full',
  },
  {
    path: 'credit-notes',
    loadChildren: () =>
      import('./credit-notes/credit-notes.module').then(
        (m) => m.CreditNotesModule
      ),
  },

  {
    path: 'online-sales-return',
    loadChildren: () =>
      import('./online-sales-return/online-sales-return.module').then(
        (m) => m.OnlineSalesReturnModule
      ),
  },

  {
    path: 'pos-sales-return',
    loadChildren: () =>
      import('./pos-sales-return/pos-sales-return.module').then(
        (m) => m.PosSalesReturnModule
      ),
  },

  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReturnRoutingModule {}
