import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages/pages.component';

const DashboardRoutes: Routes = [
      {
        path: '',
        redirectTo:'overview',
        pathMatch:'full'
      },
      {
        path: 'overview',
        loadChildren:()=> import('./overview/overview.module').then(m=>m.OverviewModule),
      },
      {
        path: 'products-catalog',
        loadChildren:()=> import('./products-catalog/products-catalog.module').then(m=>m.ProductsCatalogModule),
      },
      {
        path: 'loose-product',
        loadChildren:()=> import('./loose-product/loose-product.module').then(m=>m.LooseCatalogModule),
      },
      {
        path: 'add-product',
        loadChildren:()=> import('./add-product/add-product.module').then(m=>m.AddProductModule),
      },
      {
        path: 'return',
        loadChildren:()=> import('./return/return.module').then(m=>m.ReturnModule),
      },
      {
        path: 'invoices',
        loadChildren:()=> import('./invoices/invoices.module').then(m=>m.InvoicesModule),
      },
      {
        path: 'categories',
        loadChildren:()=> import('./categories/categories.module').then(m=>m.CategoriesModule),
      },
      {
        path: 'pos',
        loadChildren:()=> import('./pos/pos.module').then(m=>m.PosModule)
      },
      {
        path: 'cashiers',
        loadChildren:()=> import('./cashiers/cashiers.module').then(m=>m.CashiersModule)
      },
      {
        path: 'sales',
        loadChildren:()=> import('./sales/sales.module').then(m=>m.SalesModule)
      },
      {
        path: 'product-inventory',
        loadChildren:()=> import('./product-inventory/product-inventory.module').then(m=>m.ProductInventoryModule)
      },
      {
        path: 'help',
        loadChildren:()=> import('./help/help.module').then(m=>m.HelpModule)
      },
      {
        path: 'settings',
        loadChildren:()=> import('./settings/settings.module').then(m=>m.SettingsModule)
      },
      {
        path: 'help',
        loadChildren:()=> import('./help/help.module').then(m=>m.HelpModule)
      },
      {
        path:'onboarding',
        loadChildren:()=>import('./onboarding/onboarding.module').then(m=>m.OnboardingModule)
      },
      {
        path:'categories',
        loadChildren:()=>import('./categories/categories.module').then(m=>m.CategoriesModule)
      },
      {
        path:'subcategories',
        loadChildren:()=>import('./subcategories/subcategories.module').then(m=>m.SubcategoriesModule)
      }
];


@NgModule({
  imports: [RouterModule.forChild(DashboardRoutes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
