import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import * as Highcharts from 'highcharts';
import { OnboardingComponent } from '../../onboarding/onboarding.component';
import { CashierService } from 'src/app/services/cashier.service';
import { Store } from 'src/app/interface/interfaces';
import { NotificationService } from 'src/app/utils/notification.service';
import { OverviewService } from 'src/app/services/overview.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class OverviewComponent implements OnInit {
  Highcharts: typeof Highcharts = Highcharts;
  Barcharts: typeof Highcharts = Highcharts;
  Linecharts: typeof Highcharts = Highcharts;
  productStockChartOptions!: Highcharts.Options
  productChartOptions!: Highcharts.Options
  categoryChartOptions!: Highcharts.Options
  salesChartOptions!: Highcharts.Options

  data:any;
  storeList: Store[] = [];
  overview: any
  salesValues: any;
  salesPeriod: any;
  topProducts!: [{ productName: string; amount: number; }];
  topCategories!: [{ categoryName: string; amount: number; }];
  topProductPeriod: any;
  topProductValues: any;
  topCategoriesValues: any;
  topCategoriesPeriod: any;
  productStock: any[] = [];
  isMonthly: boolean = true
  isWeekly: boolean = false
  isYearly: boolean = false

  constructor(private notify: NotificationService, private overviewService: OverviewService,
      private cashierService: CashierService, public dialog: MatDialog ){

  }

  ngOnInit(){
    this.dialog.open(OnboardingComponent, {
    })
    this.getStores()
    this.getGeneralOverview('month')
    this.getTopCategoryOverview()
    this.getTopProductOverview()
  }

  drawProductStockChart(){
    this.productStockChartOptions= {
      chart:{
        type:'donut'
      },
      title:{
        text:''
      },
      xAxis:{
        categories: this.productStock.map((item: any) => item.name),
        title:{
          text:'Years'
        }
      },
      yAxis:{
        title:{
          text:'Amount'
        }
      },
      series:[{
        name:'Amount',
        type:'pie',
        innerSize: '60%',
        data: this.productStock
      }],
      credits: {
        enabled: false
      }
    };
  }
  date = new Date()
  drawLineChart(){
    this.salesChartOptions = {
      chart:{
        type:'line'
      },
      title:{
        text:''
      },
      xAxis:{
        categories:this.salesPeriod,
        title:{
          text:'Years'
        }
      },
      yAxis:{
        title:{
          text:'Amount'
        }
      },
      series:[{
        name:'Amount',
        type:'line',
        data: this.salesValues
      }],
      credits: {
        enabled: false
      }

    };
  }

  drawProductChart(){
    this.productChartOptions= {
      chart:{
        type:'bar'
      },
      title:{
        text:''
      },
      xAxis:{
        categories:this.topProductPeriod,
        title:{
          text:'Years'
        }
      },
      yAxis:{
        title:{
          text:'Amount'
        }
      },
      series:[{
        name:'Amount',
        type:'bar',
        data: this.topProductValues
      }],
      credits: {
        enabled: false
      }
    };
  }

  drawCategoryChart(){
    this.categoryChartOptions= {
      chart:{
        type:'bar'
      },
      title:{
        text:''
      },
      xAxis:{
        categories:this.topCategoriesPeriod,
        title:{
          text:'Years'
        }
      },
      yAxis:{
        title:{
          text:'Amount'
        }
      },
      series:[{
        name:'Amount',
        type:'bar',
        data: this.topCategoriesValues
      }],
      credits: {
        enabled: false
      }
    };
  }

  convertOverviewData(value:any){
    this.salesPeriod = value.map((graph:any) => graph.periodType);
    this.salesValues = value.map((graph:any) => graph.amount);
    setTimeout(() => {
       this.drawLineChart()
    }, 1500);
  }

  convertTopProductsData(value:any){
    this.topProductPeriod = value.map((graph:any) => graph.productName);
    this.topProductValues = value.map((graph:any) => graph.amount);
    setTimeout(() => {
       this.drawProductChart()
    }, 1500);
  }

  convertTopCategoriesData(value:any){
    this.topCategoriesPeriod = value.map((graph:any) => graph.categoryName);
    this.topCategoriesValues = value.map((graph:any) => graph.amount);
    setTimeout(() => {
       this.drawCategoryChart()
    }, 1500);
  }

  getStores(){
    this.cashierService.getStoreList('1').subscribe(response => {
      let res = response
      if( response.responseCode == 200 ){
        this.storeList = res.data
      }
    },(error) => {
        this.notify.showError(error.error.message);
      },() => {} )
  }

  getGeneralOverview(period: string){
    if(period == "month"){
      this.isMonthly = true
      this.isWeekly = false
      this.isYearly = false
    } else if(period == "week"){
      this.isMonthly = false
      this.isWeekly = true
      this.isYearly = false
    }else {
      this.isMonthly = false
      this.isWeekly = false
      this.isYearly = true
    }
    let req = {
      periodType: period,
      storeId: "1"
    }
    this.overviewService.getGeneralOverview(req).subscribe(response => {
      let res = response
      if( response.statusCode == 200 ){
        this.overview = res.data
        this.productStock  = []
        for(let key in this.overview.productAndStockDetails){
          this.productStock.push({name: [key], y:this.overview.productAndStockDetails[key]})
        }
        this.drawProductStockChart()
        this.convertOverviewData(this.overview.overviewSalesGraghsData)
      }
    },(error) => {
        this.notify.showError(error.error.message);
      },() => {} )
  }

  getTopProductOverview(){
    let req = {
      periodType: "week",
      storeId: "1"
    }
    this.overviewService.getTopProductsOverview(req).subscribe(response => {
      let res = response
      if( response.statusCode == 200 ){
        this.topProducts = res.data
        this.convertTopProductsData(this.topProducts)
      }
    },(error) => {
        this.notify.showError(error.error.message);
      },() => {} )
  }

  getTopCategoryOverview(){
    let req = {
      periodType: "week",
      storeId: "1"
    }
    this.overviewService.getTopCategoriesOverview(req).subscribe(response => {
      let res = response
      if( response.statusCode == 200 ){
        this.topCategories = res.data
        this.convertTopCategoriesData(this.topCategories)
      }
    },(error) => {
        this.notify.showError(error.error.message);
      },() => {} )
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {

  }

  chartCallback: Highcharts.ChartCallbackFunction = function (chart): void {
    setTimeout(() => {
        chart.reflow();
    },0);
  }
}
