import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages/pages.component';
import { RouterModule } from '@angular/router';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared';
import { PagesRoutingModule } from './pages.routes';
import { OnboardingComponent } from './onboarding/onboarding.component';
import { MatDialogModule } from '@angular/material/dialog';



@NgModule({
  declarations: [
    PagesComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    CoreModule,
    SharedModule,
    PagesRoutingModule,
    MatDialogModule
  ]
})
export class PagesModule { }
