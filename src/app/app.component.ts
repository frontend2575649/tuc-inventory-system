import {Component} from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {ApiService} from './services/api.service';
import {Router} from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  intervalTime: any;
  title = 'inventory';

  constructor(
    private apiService: ApiService,
    private router: Router,
    private bnIdle: BnNgIdleService,
    public _dataService: DataService,
  ) {

  }

  ngOnInit(): void {
    

    this.bnIdle.startWatching(600).subscribe((isTimedOut: boolean) => {
      if (isTimedOut) {
        this._dataService.DeleteStorage('currentUserData');
        this.router.navigate(['/auth/login']);
      }
    });
    //::TODO:: this needs to be refactored as a middleware
    // this.router.navigate(['/auth/login']);
  }

}
