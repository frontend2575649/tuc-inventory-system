import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IconsModule } from './shared/icons.module';
import {MatNativeDateModule} from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDialogModule} from '@angular/material/dialog';
import { SharedModule } from './shared';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { FilterPipe } from './utils/filter.pipe';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LOADING_BAR_CONFIG } from '@ngx-loading-bar/core';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,LoadingBarHttpClientModule,
    BrowserAnimationsModule,MatFormFieldModule,MatStepperModule,
    IconsModule,FormsModule,ReactiveFormsModule,MatNativeDateModule,
    BrowserAnimationsModule,HttpClientModule,
    ToastrModule.forRoot(
      {
        preventDuplicates: true,
      }),
    IconsModule,
    HttpClientModule,
    SharedModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-top-right',
    }),
  ],
  providers: [DatePipe,{ provide: LOADING_BAR_CONFIG, useValue: { latencyThreshold: 100 } }],
  bootstrap: [AppComponent]
})
export class AppModule { }
