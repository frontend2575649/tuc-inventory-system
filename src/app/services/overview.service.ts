import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { environment } from 'src/environments/environment';
import { GeneralOverviewListResponse, TopCategoryResponse, TopProductResponse } from '../interface/interfaces';

@Injectable({
  providedIn: 'root'
})
export class OverviewService extends BaseService {
  overviewUrl = environment.baseUrl + "overview";


  getGeneralOverview(req: any) {
    var url = `${this.overviewUrl}/getGeneralOverview?PeriodType=${req.periodType}&StoreId=${req.storeId}`;
    return this.get<GeneralOverviewListResponse>(url);
  }

  getTopProductsOverview(req: any) {
    var url = `${this.overviewUrl}/getTopProductOverview?PeriodType=${req.periodType}&StoreId=${req.storeId}`;
    return this.get<TopProductResponse>(url);
  }

  getTopCategoriesOverview(req: any) {
    var url = `${this.overviewUrl}/getTopCategoryOverview?PeriodType=${req.periodType}&StoreId=${req.storeId}`;
    return this.get<TopCategoryResponse>(url);
  }


}
