import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Responses } from '../interface/httpResponse';
import { StorageService } from '../utils/storage.service';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  genericError:string = "Some Error occcured, Please contact Administrator for the Errors"

  httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json",
                              "Authorization": `Bearer ${this.store.getSession()}`}),
                              body: {}
  };
  httpOptionsNotoken = {
    headers: new HttpHeaders({ "Content-Type": "application/json" }),
  };

    constructor(public http: HttpClient, private store: StorageService) {
    }

    get<T>(url: string): Observable<T> {
      let response = this.http
        .get<Responses & T>(url, this.httpOptionsNotoken);
        return response
    }

    post(url: string, body: object){
      let response = this.http
        .post<any>(url, body, this.httpOptionsNotoken);
      return response
    }

    patch(url: string, body: object){
      let response = this.http
        .patch<any>(url, body, this.httpOptionsNotoken);
      return response
    }

    put(url: string, body: object){
      let response = this.http
        .put<any>(url, body, this.httpOptionsNotoken);
      return response
    }

    delete(url: string, body?:any){
      this.httpOptions.body = body
      return this.http
        .delete<any>(url, this.httpOptionsNotoken);
    }


}
