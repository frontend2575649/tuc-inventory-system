import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BaseService } from './base.service';
import { posSalesReturnRes, CreditNoteResponse, CreditNoteDetails } from '../interface/interfaces';

@Injectable({
    providedIn: 'root'
  })
  export class ReturnsService extends BaseService {
    returnsUrl = environment.baseUrl + "returns";

    getSalesReturnsByMerchant(merchantId: number){
      var url = `${this.returnsUrl}/getSalesReturnsByMerchant?merchantId=${merchantId}`;
  
      return this.get<posSalesReturnRes>(url);
    }

    getCreditNotes(merchantId: number){
      var url = `${this.returnsUrl}/getCreditNotesByMerchant?merchantId=${merchantId}`;
  
      return this.get<CreditNoteResponse>(url);
    }

    getCreditNotesDetails(id: number){
      var url = `${this.returnsUrl}/getCreditNoteDetail?creditNoteId=${id}`;
  
      return this.get<CreditNoteDetails>(url);
    }

    
  
  
    // getCashierList() {
    //   var url = `${this.returnsUrl}/getCashiers`;
  
    //   return this.get<CashierListResponse>(url);
    // }
  
    // getCashierById(id:string){
    //   var url = `${this.returnsUrl}?id=${id}`;
  
    //   return this.get<CashierDetailsResponse>(url);
    // }
  
    // postCashier(req: CashierData){
    //   var url = `${this.returnsUrl}`;
  
    //   return this.post(url, req);
    // }


  
  }
  