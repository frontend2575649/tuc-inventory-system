import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  constructor() { }

  getQueryString(filterData:any){
    let queryString = "?"+ new URLSearchParams(filterData).toString().replace(/\+/g, '%20')
    return queryString;
  }
}
