import { Injectable } from '@angular/core';
import { any } from 'underscore';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private businessDetails = new BehaviorSubject(null);
  availableData = this.businessDetails.asObservable();

  private sidebarOpen = new BehaviorSubject(null);
  isSideBarOpen = this.sidebarOpen.asObservable();

  constructor(public _router: Router) {
    this.setCurrentUserData();
  }
  public appConfig = {
    Color_Green: '#22b66e',
    Color_Red: '#f14d4d',
    Color_Blue: '#0168fa',
    Color_Grey: '#989c9e',
  };

  acquirerId: any;
  userId: any;
  loggedInUserName: any;
  ActivePageName: any = '';
  selectedCampaignId: number = 0;

  updateBusinessDetails(data: any) {
    this.businessDetails.next(data);
  }

  changeSideBar(data: any) {
    this.sidebarOpen.next(data);
  }

  SaveStorage(StorageName: any, StorageValue: any) {
    try {
      var StringV = btoa(
        unescape(encodeURIComponent(JSON.stringify(StorageValue)))
      );
      localStorage.setItem(StorageName, StringV);
      return true;
    } catch (e) {
      alert(e);
      return false;
    }
  }
  GetStorage(StorageName: any) {
    var StorageValue = localStorage.getItem(StorageName);
    if (StorageValue != undefined) {
      if (StorageValue != null) {
        return JSON.parse(atob(StorageValue));
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
  DeleteStorage(StorageName: any) {
    localStorage.removeItem(StorageName);

    return true;
  }

  setCurrentUserData() {
    let userData = this.GetStorage('currentUserData');
    if (userData) {
      this.acquirerId = userData.id;
      this.userId = userData.userId;
      this.loggedInUserName = userData.bankName ? userData.bankName : userData.displayName
    }
  }
  defaultImage(event: any) {
    event.target.src = '../../assets/img/Signup/cardIconone.png';
  }

  PreventText(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (
      (event.keyCode != 8 && !pattern.test(inputChar) && event.which < 48) ||
      event.which > 57
    ) {
      event.preventDefault();
    }
  }
}
