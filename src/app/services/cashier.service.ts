import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BaseService } from './base.service';
import {CashierCount, CashierData, CashierDetailsResponse, CashierListResponse, InvoiceListResponse, PosListResponse, StoreListResponse} from '../interface/interfaces';

@Injectable({
  providedIn: 'root'
})
export class CashierService extends BaseService {
  cashierUrl = environment.baseUrl + "cashier";
  invoiceUrl = environment.baseUrl + "sales";
  storeUrl = environment.baseUrl + "stores";
  posUrl = environment.baseUrl + "pos";


  getCashierList(req: any) {
    var url = `${this.cashierUrl}/getCashiers?dateFrom=${req.dateFrom}&dateTo=${req.dateTo}&searchKey=${req.searchKey}&status=${req.status}&sortKey=${req.sortKey}`;
    return this.get<CashierListResponse>(url);
  }

  getStoreList(merchantId: any) {
    var url = `${this.storeUrl}/getStores?MerchantId=${merchantId}`;
    return this.get<StoreListResponse>(url);
  }


  getPosList(id: number){
    var url = `${this.posUrl}/getTerminals?merchantId=1`;
    return this.get<PosListResponse>(url);
  }

  getCashierCounts(){
    var url = `${this.cashierUrl}/getCashiersCount`
    return this.get<CashierCount>(url)
  }

  getCashierOverview(id: any){
    var url = `${this.invoiceUrl}/getTotalData?cid=${id}`
    return this.get<any>(url)
  }

  getCashierById(id:string){
    var url = `${this.cashierUrl}?id=${id}`;

    return this.get<CashierDetailsResponse>(url);
  }
  getCashierByUserId(userId:string){
    var url = `${this.cashierUrl}/getCashierByUserId?userId=${userId}`;
    return this.get<any>(url);
  }

  getInvoiceByCashier(cashierCode:string){
    var url = `${this.invoiceUrl}/getInvoicebycid?cashierCode=${cashierCode}`;
    return this.get<InvoiceListResponse>(url);
  }

  postCashier(req: CashierData){
    var url = `${this.cashierUrl}`;

    return this.post(url, req);
  }

}
