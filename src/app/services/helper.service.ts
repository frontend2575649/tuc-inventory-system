import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
@Injectable({
  providedIn: 'root'
})


export class HelperService {
  public Center: any = 'center';
  public AppConfig = {
    Color_Green: '#22b66e',
    Color_Red: '#f14d4d',
    Color_Blue: '#0168fa',
    Color_Grey: '#989c9e',
 
    DataType: {
      Text: 'text',
      Number: 'number',
      Date: 'date',
      Decimal: 'decimal',
    },
    ListRecordLimit: [10, 25, 30, 50, 90, 100],
    
    HostConnect: '',
  
  };
  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private datePipe: DatePipe
  ) { }


  ConvertToTimeStamp(datestring:string){
    return (new Date(datestring)).getTime() / 1000;
  }

  ExtractBase64String(data:string){
    if (data.startsWith("data:")) {
        let dataArray = data.split(',');
        return dataArray[1];
    }
    return data;
  }
  GetExtensionName(fileName:string){
    let nameArray = fileName.split('.');
    return nameArray[1];
  }
  GetFileName(fileName:string){
    let nameArray = fileName.split('.');
    return nameArray[0];
  }

  ValidateFiles(files:any){
    console.log(files);
    if (files.invalidFiles.length > 0) {
      return false;
    }
    return true;
  }

  MapTo<T>(data:any){
    let stringData = JSON.stringify(data);
    return JSON.parse(stringData) as T;
  }

  formatDate2(date: string){
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    let year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  ExportData(data:any[], filename:string, dataTitle: string){
    let dateSting  =  this.ConvertToTimeStamp((new Date()).toISOString());
          const options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalSeparator: '.',
            showLabels: true,
            showTitle: true,
            title: dataTitle,
            useTextFile: false,
            useBom: true,
            useKeysAsHeaders: true,
            filename: `Generated_${filename}_${dateSting}`
            // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
          };
          // const csvExporter = new ExportToCsv(options);
          // csvExporter.generateCsv(data);
  }

 ConvertToNumber(numWithString: string):number
 {
  let response = numWithString.split(',').join("");
  return  response as any as number;
 }

  DateToIso(data:string){
    const parsedDate = new Date(data);
    const formattedDate = parsedDate.toLocaleString("en-US", { timeZone: "UTC" });
    const isoDate = new Date(formattedDate).toISOString();
      return isoDate;
  }
  DateStringToFull(data:any){
    const date = new Date(data);
    const formattedDate = this.datePipe.transform(date, 'yyyy-MM-dd');
    return formattedDate
  }
  numbersOnly(event: KeyboardEvent): boolean{
    const charCode = event.which ? event.which : event.keyCode;
    if(charCode > 31 && (charCode < 48 || charCode > 57))return false;
    return true;
   }
}

enum HostType {
  Live,
  Test,
  Tech,
  Dev,
}
