import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BackendGeneralFormat, ATax, ATaxToBeCreated, ReturnPolicyTobeCreated, CreatedPolicy, SearchParams, Product, ACategoryFromStore, ExtendedCreatedPolicy } from '../interface/interfaces';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, timeout } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
  export class ReturnPoliciesService  {
    private returnPoliciesUrl = {
      getReturnPolicies: environment.baseUrl + "policies/getPolicies",
      createPolicy: environment.baseUrl + 'policies/createPolicy',
      getAPolicy: environment.baseUrl + 'policies/getPolicy',
      deleteAPolicy: environment.baseUrl + 'policies/deletePolicy',
      updatePolicy: environment.baseUrl + 'policies/updatePolicy',
      applyPolicyToProducts: environment.baseUrl + 'policies/applyOnProduct'
    } 
    constructor(private http: HttpClient){}


    getReturnPolicies(merchantId: any): Observable<BackendGeneralFormat<CreatedPolicy[]>>{
        const params = new HttpParams().set('merchantId', merchantId);
        return this.http.get<BackendGeneralFormat<CreatedPolicy[]>>(this.returnPoliciesUrl.getReturnPolicies, {params});
      }

      fetchAReturnPolicy(policyId: any): Observable<BackendGeneralFormat<ExtendedCreatedPolicy>>{
        const params = new HttpParams().set('policyId', policyId);
        return this.http.get<BackendGeneralFormat<ExtendedCreatedPolicy>>(this.returnPoliciesUrl.getAPolicy, {params});
      }

    createAReturnPolicy(req: ReturnPolicyTobeCreated){
        return this.http.post<BackendGeneralFormat>(this.returnPoliciesUrl.createPolicy, req);
    }
    applyPolicyOnAProduct(req: {merchantId: number,policyId:number,productds: number[]}){
      return this.http.put<BackendGeneralFormat>(this.returnPoliciesUrl.applyPolicyToProducts, req);
    }

    updateAPolicy(req: ReturnPolicyTobeCreated & {merchantId: any, policyId: any} ){
      return this.http.put<BackendGeneralFormat>(this.returnPoliciesUrl.updatePolicy, req);
    }

    deleteAPolicy(merchantId: any, policyId: any){
      const params = new HttpParams().set('merchantId', merchantId).set('policyId', policyId)
      return this.http.delete<BackendGeneralFormat>(this.returnPoliciesUrl.deleteAPolicy, {params});
    }

    getProducts(req: SearchParams):Observable<BackendGeneralFormat<Product[]>>{
      const params = Object.keys(req).reduce((prev: HttpParams, elem: string) => prev.set(elem, (req as any)[elem]),new HttpParams());
      return this.http.get<BackendGeneralFormat<Product[]>>(`${environment.baseUrl}product/getProducts`, {params})
      .pipe(timeout(100000))
    }
  
    getCategories(merchantId?: any):Observable<BackendGeneralFormat<ACategoryFromStore[]>>{
      const params = new HttpParams().set('merchantId', merchantId ? merchantId : 1)
      return this.http.get<BackendGeneralFormat<ACategoryFromStore[]>>(`${environment.baseUrl}category/getCategories`, {params})
      .pipe(timeout(100000))
    }

  
  }
  