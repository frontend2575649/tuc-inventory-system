import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BaseService } from './base.service';
import { posSalesReturnRes, CreditNoteResponse, CreditNoteDetails } from '../interface/interfaces';

@Injectable({
    providedIn: 'root'
  })
  export class AddProductService  extends BaseService {
    productUrl = environment.baseUrl + "products";

    addProducts(req: any){
    //   var url = `${this.returnsUrl}/getCreditNoteDetail?creditNoteId=${id}`;
  
      return this.post(this.productUrl, req);
    }

  
  }
  