import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, pipe, throwError } from 'rxjs';
import { DataService } from './data.service';
import { FilterService } from './filter.service';
import { NotificationService } from './notification.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  environment: any;

  httpOptions: any;
  token?: string;
  refreshToken?: string;
  tokenExpiration!: number;
  tokenDetails: any;
  result: string[] = [];

  develope_branch = {
    apiURL: 'https://api-inventory.thankucash.dev/api/v1/',
  };
  qa_branch = {
    apiURL: 'https://api-inventory.thankucash.tech/api/v1/',
  };


  config(): any {
    // const isLocal = (window.location.host as string).includes('localhost');
    const host = window.location.host;
    // if (host == 'localhost') return this.qa_branch;
    if (host == 'inventory.thankucash.dev') return this.develope_branch;
    else if (host == 'inventory.thankucash.tech') return this.qa_branch;
  
    else {
      return this.develope_branch;
    }
  }

  constructor(
    private http: HttpClient,
    private _filter: FilterService,
    private _dataService: DataService,
    private _notification: NotificationService
  ) {
    this.environment = this.config();
    //  this.refreshUserToken();
    this.getHttpOptions();
   
  }



  getHttpOptions(isFormData: boolean = false, optionalParams?: Record<string, any>): any {
    const currentUserData = this._dataService.GetStorage('currentUserData');
    let headers: any = new HttpHeaders({
      Accept: 'application/json',
    });
    isFormData
      ? headers.set('Content-Type', 'multipart/form-data')
      : headers.set('Content-Type', 'application/json');
    if (currentUserData && currentUserData.jwtToken) {
      headers = { Authorization: `Bearer ${currentUserData.jwtToken}` };
    }
    let params: HttpParams;
    if (optionalParams) {
      params = Object.keys(optionalParams).reduce((prev, curr) => prev.set(curr, (optionalParams as any)[curr]), new HttpParams());
      this.httpOptions = { headers, params };
      return;
    }
    return this.httpOptions = { headers };
  }




  getData(taskUrl: any) {
    this.getHttpOptions();
    return this.http
      .get<any>(this.environment.apiURL + taskUrl, this.httpOptions)
      .pipe(
        catchError((error) => {
          this._notification.HandleException(error);
          throw error;
        })
      );
  }

  getDatawithFilter(taskUrl: any, filterData: any) {
    this.getHttpOptions();
    let queryParams = this._filter.getQueryString(filterData);
    // let options = {  headers: this.httpOptions };
    return this.http
      .get<any>(
        this.environment.apiURL + taskUrl + queryParams,
        this.httpOptions
      )
      .pipe(
        catchError((error) => {
          // this._notification.HandleException(error);
          this._notification.toastrInfo(error.error.message);
          throw error;
        })
      );
  }
  /**
   * @param {any} requestData request body
   * @param {string} taskUrl api url
   * @param {boolean} isFormData if true then request body is multipart/form-data otherwise application/json
   */
  postData(
    requestData: any,
    taskUrl: any,
    isFormData: boolean = false
  ): Observable<any> {
    this.getHttpOptions(isFormData);
    return this.http
      .post<any>(
        this.environment.apiURL + taskUrl,
        requestData,
        this.httpOptions
      )
      .pipe(
        catchError((error) => {
          this._notification.toastrError(error.error.message);
          throw error;
        })
      );
  }

  deleteData(taskUrl: any, filterData: any): Observable<any> {
    this.getHttpOptions();
    let queryParams = this._filter.getQueryString(filterData);
    return this.http
      .delete<any>(
        this.environment.apiURL + taskUrl + queryParams,
        this.httpOptions
      )
      .pipe(
        catchError((error) => {
          this._notification.toastrError(error.error.message);
          throw error;
        })
      );
  }

  /**
   * @param {any} requestData request body
   * @param {string} taskUrl api url
   * @param {boolean} isFormData if true then request body is multipart/form-data otherwise application/json
   */
  patchData(
    requestData: any,
    taskUrl: any,
    isFormData: boolean = false
  ): Observable<any> {
    this.getHttpOptions(isFormData);
    return this.http
      .patch<any>(
        this.environment.apiURL + taskUrl,
        requestData,
        this.httpOptions
      )
      .pipe(
        catchError((error) => {
          this._notification.toastrError(error.error.message);
          throw error;
        })
      );
  }


  putData(requestData: any, taskUrl: any, isFormData: boolean = false, extras?: Record<string, any>): Observable<any> {
    this.getHttpOptions(isFormData, extras);
    return this.http.put<any>(this.environment.apiURL + taskUrl, requestData, this.httpOptions)
      .pipe(catchError(error => {
        this._notification.toastrError(error.error.message);
        throw error;
      }));
  }


  downloadFile(taskUrl: any, filterData: any) {
    let queryParams = this._filter.getQueryString(filterData);
    const currentUserData = this._dataService.GetStorage('currentUserData');
    let authKey = `Bearer ${currentUserData.jwtToken}`;
    return this.http.get(this.environment.apiURL + taskUrl + queryParams,{ responseType: 'blob',
    headers: new HttpHeaders({
      'Authorization': authKey || ''
    })
  }).pipe(catchError(error => {
      // this._notification.HandleException(error);
      this._notification.toastrInfo(error.error.message)
      throw error;
    }));;
  }
}


