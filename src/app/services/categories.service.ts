import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BaseService } from './base.service';
import { GetCategoriesRes, GetSubCategoriesRes } from '../interface/interfaces';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService extends BaseService {
  categoryUrl = environment.baseUrl + "category";
  subCategoryUrl = environment.baseUrl + "subCagetory";
  APIURL = 'https://api-inventory.thankucash.dev/api/v1/' 




  getCategories(merchantId: number){
    var url = `${this.categoryUrl}/getCategories?merchantId=${merchantId}`;

    return this.get<GetCategoriesRes>(url);
  }

  getSubCategories(merchantId: number){
    var url = `${this.subCategoryUrl}/getCategories?merchantId=${merchantId}`;

    return this.get<GetSubCategoriesRes>(url);
  }

  createCategories() {
    var url = `${this.categoryUrl}/createCategory`;
  }

  createSubCategories() {
    var url = `${this.subCategoryUrl}/createCategory`;
  }

  postData(url: string, body:any): Observable<any> {
    return this.http.post(this.APIURL+url, body);
}

putData(url: string, body:any): Observable<any> {
  return this.http.put(this.APIURL+url, body);
}
}
