import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BackendGeneralFormat, TaxOnProduct, EnableNotificationReqBody, TaxAndStockSettings } from '../interface/interfaces';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
    providedIn: 'root'
  })
  export class GeneralSettingsService  {
    private generalSettings = {
      enableTracking: environment.baseUrl + "settings/enableTracking",
      enableNotification: environment.baseUrl + 'settings/notification',
      enableTaxing: environment.baseUrl + 'settings/taxManagement',
      getMerchantSettings: environment.baseUrl + 'settings/getSettings',
      getTaxAndStockSettings: environment.baseUrl + 'settings/getTaxAndStockSettings'
    } 
    constructor(private http: HttpClient){}

   
  enableNotification(merchantId: string, req: EnableNotificationReqBody): Observable<BackendGeneralFormat>{
    const params = new HttpParams().set('merchantId', merchantId);
    return this.http.post<BackendGeneralFormat>(this.generalSettings.enableNotification, req, {params});
  }

  getPreviousSettings(merchantId: any): Observable<BackendGeneralFormat<EnableNotificationReqBody>>{
    const params = new HttpParams().set('merchantId', merchantId);
     return this.http.get<BackendGeneralFormat<EnableNotificationReqBody>>(this.generalSettings.getMerchantSettings, {params})
    }

    taxAndStockSettings(merchantId: any): Observable<BackendGeneralFormat<TaxAndStockSettings>>{
      const params = new HttpParams().set('merchantId', merchantId);
       return this.http.get<BackendGeneralFormat<TaxAndStockSettings>>(this.generalSettings.getTaxAndStockSettings, {params})
      }

//   createATax(req: ATaxToBeCreated): Observable<any>{
//     return this.http.post<BackendGeneralFormat>(this.generalSettings.createTax, req);
//   }

    enableTracking(req: {merchantId: number,storeIds: number[], status: boolean}): Observable<BackendGeneralFormat>{
    return this.http.post<BackendGeneralFormat>(this.generalSettings.enableTracking, req)
    }

    enableTaxingOnProduct(req: {merchantId: number,enableTaxOnProduct: boolean, taxInfo: TaxOnProduct}): Observable<BackendGeneralFormat>{
        return this.http.post<BackendGeneralFormat>(this.generalSettings.enableTaxing, req)
    }
  }
  