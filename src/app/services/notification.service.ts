import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import swal, { SweetAlertOptions } from 'sweetalert2';
import { HelperService } from './helper.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  IsFormProcessing = false;
  constructor(
    public router: Router,
    private toastr: ToastrService,
    private _HelperService: HelperService
  ) { }

  // NotifySuccess(Message:any) {
  //   swal.fire({
  //     text: Message,
  //     customClass: "",
  //     showCancelButton: false,
  //     showConfirmButton:true,
  //     confirmButtonColor: this._HelperService.AppConfig.Color_Green,
  //     confirmButtonText: "Close",
  //     icon: 'success',
  //     // timer: 2500
  //   });
  // }

  // NotifyError(Message:any) {
  //   swal.fire({
  //     text: Message,
  //     customClass: "",
  //     showCancelButton: false,
  //     showConfirmButton:true,
  //     confirmButtonColor: this._HelperService.AppConfig.Color_Green,
  //     confirmButtonText: "Close",
  //     icon: 'error',
  //   });
  // }

  NotifySuccess(Message: any) {
    console.log(Message)
    swal.fire({
      icon: 'success',
      title: "Success!!!",
      text: Message,
    })
  }
  NotifyError(Message: any) {
    swal.fire({
      icon: 'error',
      title: "Error!",
      text: Message,
    })
  }

  NotifyInfo(Message: any) {
    swal.fire({
      text: Message,
      customClass: "",
      showCancelButton: false,
      showConfirmButton: false,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      confirmButtonText: "Close",
      icon: 'info',
      // timer: 2500
    });
  }

  modifiedNotificationService(options: SweetAlertOptions){
    swal.fire(options);
  }

  toastrInfo(message: string) {
    this.toastr.info(message);
  }
  toastrError(message: string) {
    this.toastr.error(message);
  }
  HandleException(Exception: HttpErrorResponse) {
    this.IsFormProcessing = false;
    if (
      Exception.status != undefined &&
      Exception.error != null &&
      Exception.status == 401
    ) {
      window.localStorage.clear();
      setTimeout(() => {
        this.router.navigate(['/auth/login']);
      }, 1500);
    } else {
      if (Exception.status == 404) {
        return
      }
      else if (Exception.error instanceof Error) {
        this.NotifyError("Sorry, error occured while connecting server:" + Exception.error.message);
      } else {
        this.NotifyError(
          Exception.error.message
          // + " Response Code -" +
          // Exception.status
        );
      }
    }
  }
}
