import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BackendGeneralFormat, ATax, ATaxToBeCreated, ATaxFromServer, AStoreLocation } from '../interface/interfaces';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
  export class TaxesService  {
    private taxesUrl = {
      getTaxes: environment.baseUrl + "taxManagement/getTaxes",
      getOneTax: environment.baseUrl + "taxManagement/getTax",
      createTax: environment.baseUrl + 'taxManagement/createTax',
      deleteTax: environment.baseUrl + 'taxManagement/deleteTax',
      editTax: environment.baseUrl + 'taxManagement/editTax',
      getLocations: environment.baseUrl + 'taxManagement/getStoreLocations',
      applyTaxToCategory: environment.baseUrl + 'taxManagement/applyTaxOnCategories',
    } 
    constructor(private http: HttpClient){}

   
  getTaxes(merchantId: any): Observable<BackendGeneralFormat<ATax[]>>{
    const params = new HttpParams().set('merchantId', merchantId);
    return this.http.get<BackendGeneralFormat<ATax[]>>(this.taxesUrl.getTaxes, {params});
  }

  getATax(taxId: any): Observable<BackendGeneralFormat<ATaxFromServer>>{
    const params = new HttpParams().set('taxId', taxId).set('merchantId', '1');
    return this.http.get<BackendGeneralFormat<ATaxFromServer>>(this.taxesUrl.getOneTax, {params});
  }

  createATax(req: ATaxToBeCreated): Observable<any>{
    return this.http.post<BackendGeneralFormat>(this.taxesUrl.createTax, req);
  }

  editATax(req: Partial<ATaxToBeCreated> & {merchantId: any, taxId: any}): Observable<any>{
    return this.http.put<BackendGeneralFormat>(this.taxesUrl.editTax, req);
  }
  
  deleteTax(merchantId:any, taxId: any){
    const params = new HttpParams().set('merchantId', merchantId).set('taxId', taxId);
    return this.http.delete<BackendGeneralFormat>(this.taxesUrl.deleteTax, {params});
  }

  applyTaxToCategories(req: {merchantId: number,taxId: number,categoryIds: number[]}):Observable<any>{
    return this.http.put(this.taxesUrl.applyTaxToCategory, req);
  }

  getStoreLocations(merchantId: any): Observable<BackendGeneralFormat<AStoreLocation[]>>{
    const params = new HttpParams().set('merchantId', merchantId);
    return this.http.get<BackendGeneralFormat<AStoreLocation[]>>(this.taxesUrl.getLocations, {params});
  }

  
  }
  