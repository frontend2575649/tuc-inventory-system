import {  HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SalesSummaryResponse } from 'src/app/interface/interfaces';

@Injectable({
    providedIn:'root'
})

export class ProductsService{

    APIURL = 'https://api-inventory.thankucash.dev/api/v1/' 
    constructor(
        private http: HttpClient,
        public router: Router
    ){}

    fetchData(url: string, params: any): Observable<any> {
        return this.http.get(this.APIURL+url, { params });
      }

    postData(url: string, body:any): Observable<any> {
        return this.http.post(this.APIURL+url, body);
    }
}