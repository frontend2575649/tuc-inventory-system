import {  HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SalesSummaryResponse } from 'src/app/interface/interfaces';

@Injectable({
    providedIn:'root'
})

export class SettingsService{

    APIURL = 'https://api-inventory.thankucash.dev/api/v1/' 
    constructor(
        private http: HttpClient,
        public router: Router
    ){}

    fetchData(url: string, params: any): Observable<any> {
        return this.http.get(this.APIURL+url, { params });
      }

      postData(url: string, body:any): Observable<any> {
        return this.http.post(this.APIURL+url, body);
      }
      putData(url: string, body:any): Observable<any> {
        return this.http.put(this.APIURL+url, body);
      }
      deleteData(url:string,param:any): Observable<any> {
       
        const params = new HttpParams()
        .set('merchantId', param.merchantId)
        .set('discountId', param.discountId);
        const options = { params: params };
        return this.http.delete(this.APIURL+url,options)
      } 
}

