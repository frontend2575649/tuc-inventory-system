import {  HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SalesSummaryResponse } from 'src/app/interface/interfaces';

@Injectable({
    providedIn:'root'
})

export class ApiService{

    APIURL = 'https://api-inventory.thankucash.dev/api/v1/' 
    constructor(
        private http: HttpClient,
        public router: Router
    )
    {}

    fetchData(baseUrl:string, url:string): Observable<any>{
       return this.http.get<any>(`${baseUrl}${url}`)
    }
    fetchDataParams(url: string, params: any): Observable<any> {
        return this.http.get(this.APIURL+url, { params });
      }
}

