import {  HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
    providedIn:'root'
})

export class StoresService{

    APIURL = 'https://api-inventory.thankucash.dev/api/v1/' 
    constructor(
        private http: HttpClient,
        public router: Router
    ){}

    fetchData(url: string, params: any): Observable<any> {
        return this.http.get(this.APIURL+url, { params });
      }

      postData(url: string, body:any): Observable<any> {
        return this.http.post(this.APIURL+url, body);
      }

}