import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './dashboard/pages/pages.component';
import { AuthGuard } from '../app/guards/auth.guard';
const routes: Routes = [
 
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: '',
    canMatch: [AuthGuard],
    component:PagesComponent,
    loadChildren:()=> import('./dashboard/pages.module').then(m=>m.PagesModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
