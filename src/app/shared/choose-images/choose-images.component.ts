import { Component } from '@angular/core';

enum Views{
  General_View= 1,
  Upload_View
}
@Component({
  selector: 'app-choose-images',
  templateUrl: './choose-images.component.html',
  styleUrls: ['./choose-images.component.scss']
})
export class ChooseImagesComponent {
  views: Views = Views.General_View
}
