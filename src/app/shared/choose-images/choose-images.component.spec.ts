import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseImagesComponent } from './choose-images.component';

describe('ChooseImagesComponent', () => {
  let component: ChooseImagesComponent;
  let fixture: ComponentFixture<ChooseImagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChooseImagesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChooseImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
