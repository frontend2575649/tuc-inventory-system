import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { IconsModule } from './icons.module';
import { ReusablesTableComponent } from './reusables/reusables-table/reusables-table.component';
import { ReusablesTableTopWithFiltersComponent } from './reusables/reusables-table-top-with-filters/reusables-table-top-with-filters.component';
import { ReusablesCardsComponent } from './reusables/reusables-cards/reusables-cards.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ReusablesGeneralSuccessNotificationComponent } from './reusables/reusables-general-success-notification/reusables-general-success-notification.component';
import {MatDialogModule} from '@angular/material/dialog';
import { ResuableCardGroupComponent } from './reusables/resuable-card-group/resuable-card-group.component';
import { TableParentComponent } from './reusables/table-parent/table-parent.component';
import { ReusablesFilterComponent } from './reusables/reusables-filter/reusables-filter.component';
import { ChooseImagesComponent } from './choose-images/choose-images.component';


@NgModule({
  declarations: [
    SidebarComponent,
    NavbarComponent,
    ReusablesTableComponent,
    ReusablesTableTopWithFiltersComponent,
    ReusablesCardsComponent,
    ReusablesGeneralSuccessNotificationComponent,
    ResuableCardGroupComponent,
    TableParentComponent,
    ReusablesFilterComponent,
    ChooseImagesComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    IconsModule,
    NgSelectModule,
    MatDialogModule
    
  ],
  exports:[
    SidebarComponent,
    NavbarComponent,
    ReusablesTableComponent,
    NgSelectModule,
    ReusablesTableTopWithFiltersComponent,
    ReusablesGeneralSuccessNotificationComponent,
    MatDialogModule,
    ReusablesCardsComponent,
    ResuableCardGroupComponent,
    TableParentComponent,
    ReusablesFilterComponent,
    ChooseImagesComponent
  ],
  providers: [MatDialogModule]
})
export class SharedModule { }
