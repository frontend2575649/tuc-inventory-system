import { Component } from '@angular/core';
import { TestData } from 'src/app/interface/interfaces';

@Component({
  selector: 'app-table-parent',
  templateUrl: './table-parent.component.html',
  styleUrls: ['./table-parent.component.scss']
})
export class TableParentComponent {
  tableData: TestData[] = [
    {
      ReferenceId: 13254,
      ReferenceKey: "bf18878177ab49e9ba7fbc36dc6a6657",
      DisplayName: "ThankUCashBs",
      ContactNumber: "2347056635295",
      EmailAddress: "paul-ominyi+7@thankucash.com",
      Address: "1 Wole Olanipekun Cl, Lekki Phase I 106104, Lekki, Lagos, Nigeria",
      MerchantId: 13253,
      MerchantKey: "b25a91f50bbe4a7686116d2a13b47e55",
      CreateDate: "2023-01-06T13:41:55",
      StatusId: 2,
      StatusCode: "default.active",
      StatusName: "Active",
      Categories: [
        {
          CategoryId: 10,
          CategoryKey: "10",
          CategoryName: "Groceries"
        }
      ]
    },
    {
      ReferenceId: 1325962,
      ReferenceKey: "010058ffae064702bd9820f6f0abb255",
      DisplayName: "New store",
      ContactNumber: "2348098998989",
      EmailAddress: "paul-ominyi+7@thankucash.com",
      Address: "131 Obafemi Awolowo Way, Oba Akran 100271, Ikeja, Lagos, Nigeria",
      MerchantId: 13253,
      MerchantKey: "b25a91f50bbe4a7686116d2a13b47e55",
      CreateDate: "2023-01-25T13:08:18",
      StatusId: 2,
      StatusCode: "default.active",
      StatusName: "Active",
      Categories: [
        {
          CategoryId: 7,
          CategoryKey: "7",
          CategoryName: "Fashion"
        }
      ]
    },
    {
      ReferenceId: 1319110,
      ReferenceKey: "180ffde1d79f4c40a23849da80d3005e",
      DisplayName: "Deal  iPhone Store",
      ContactNumber: "2348098779259",
      EmailAddress: "paul-ominyi+7@thankucash.com",
      Address: "H8VP+JG5, Ikeja 101233, Ikeja, Lagos, Nigeria",
      MerchantId: 13253,
      MerchantKey: "b25a91f50bbe4a7686116d2a13b47e55",
      CreateDate: "2023-01-19T10:45:50",
      StatusId: 2,
      StatusCode: "default.active",
      StatusName: "Active",
      Categories: [
        {
          CategoryId: 222,
          CategoryKey: "58861be927c642f9bcf55d64f02efbc7",
          CategoryName: "iphone"
        }
      ]
    }
  ]
}
