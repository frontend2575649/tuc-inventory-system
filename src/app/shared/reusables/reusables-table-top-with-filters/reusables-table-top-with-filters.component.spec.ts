import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReusablesTableTopWithFiltersComponent } from './reusables-table-top-with-filters.component';

describe('ReusablesTableTopWithFiltersComponent', () => {
  let component: ReusablesTableTopWithFiltersComponent;
  let fixture: ComponentFixture<ReusablesTableTopWithFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReusablesTableTopWithFiltersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReusablesTableTopWithFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
