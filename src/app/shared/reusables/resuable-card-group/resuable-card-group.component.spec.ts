import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResuableCardGroupComponent } from './resuable-card-group.component';

describe('ResuableCardGroupComponent', () => {
  let component: ResuableCardGroupComponent;
  let fixture: ComponentFixture<ResuableCardGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResuableCardGroupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResuableCardGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
