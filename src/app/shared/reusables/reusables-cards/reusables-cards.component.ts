import { Component, Input } from '@angular/core';
import { CardDetails } from 'src/app/interface/interfaces';


@Component({
  selector: 'app-reusables-cards',
  templateUrl: './reusables-cards.component.html',
  styleUrls: ['./reusables-cards.component.scss']
})
export class ReusablesCardsComponent {
 @Input() data!: CardDetails
}
