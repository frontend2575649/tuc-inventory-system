import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReusablesCardsComponent } from './reusables-cards.component';

describe('ReusablesCardsComponent', () => {
  let component: ReusablesCardsComponent;
  let fixture: ComponentFixture<ReusablesCardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReusablesCardsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReusablesCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
