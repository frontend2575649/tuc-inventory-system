import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReusablesGeneralSuccessNotificationComponent } from './reusables-general-success-notification.component';

describe('ReusablesGeneralSuccessNotificationComponent', () => {
  let component: ReusablesGeneralSuccessNotificationComponent;
  let fixture: ComponentFixture<ReusablesGeneralSuccessNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReusablesGeneralSuccessNotificationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReusablesGeneralSuccessNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
