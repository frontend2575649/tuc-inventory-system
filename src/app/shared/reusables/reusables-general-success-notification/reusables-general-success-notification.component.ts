import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import { NotificationData } from 'src/app/interface/interfaces';

@Component({
  selector: 'app-reusables-general-success-notification',
  templateUrl: './reusables-general-success-notification.component.html',
  styleUrls: ['./reusables-general-success-notification.component.scss']
})
export class ReusablesGeneralSuccessNotificationComponent {
  constructor( @Inject(MAT_DIALOG_DATA) public data: NotificationData,){}
}
