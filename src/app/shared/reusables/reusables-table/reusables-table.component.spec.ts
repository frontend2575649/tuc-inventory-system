import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReusablesTableComponent } from './reusables-table.component';

describe('ReusablesTableComponent', () => {
  let component: ReusablesTableComponent;
  let fixture: ComponentFixture<ReusablesTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReusablesTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReusablesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
