import { Component, ContentChild, Input, TemplateRef } from '@angular/core';
import { TestData } from 'src/app/interface/interfaces';

@Component({
  selector: 'app-reusables-table',
  templateUrl: './reusables-table.component.html',
  styleUrls: ['./reusables-table.component.scss']
})
export class ReusablesTableComponent {
  @Input() tableData!: any[];
  @ContentChild('headers') headers!: TemplateRef<any>;
  @ContentChild('rows') rows!: TemplateRef<any>;
}
