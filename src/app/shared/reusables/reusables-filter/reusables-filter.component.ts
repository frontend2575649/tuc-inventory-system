import { Component } from '@angular/core';

@Component({
  selector: 'app-reusables-filter',
  templateUrl: './reusables-filter.component.html',
  styleUrls: ['./reusables-filter.component.scss']
})
export class ReusablesFilterComponent {
  showDropDown: boolean = false;
  showAdvancedSearchParams: boolean = false;
}
