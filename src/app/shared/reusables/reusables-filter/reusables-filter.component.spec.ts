import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReusablesFilterComponent } from './reusables-filter.component';

describe('ReusablesFilterComponent', () => {
  let component: ReusablesFilterComponent;
  let fixture: ComponentFixture<ReusablesFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReusablesFilterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReusablesFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
