import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  collapseBar()
  {
      let body:any = document.querySelector('body')
      let main:any = document.querySelector('.main-wrapper')
      if(body.classList.contains('isCollapsed')){
          body.classList.remove('isCollapsed')
      }else{
        body.classList.add("isCollapsed");
        main.classList.add("isCollapsed");
      }
  }

  closeSideBar(){
    let body:any = document.querySelector('body')
    body.classList.toggle('isHover')
  }

  confirmLogout(){

  }

  CollapsedHover(data:any){
      let body:any = document.querySelector('body')
      body.classList.add('isHover')
  }

  UnCollapsedHover()
  {
    let body:any = document.querySelector('body')
    if(body.classList.contains('isCollapsed')){
      body.classList.remove('isHover')
    }
  }

}
