import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService{

  setSession(token: string){
    localStorage.setItem("token-session-TIM", token);
  }
  getSession(){
    return localStorage.getItem("token-session-TIM");
  }
  setLoginUser(isLogin: string){
    return localStorage.setItem("isLogin", isLogin)
  }

  isUserLoggedin(){
    if(localStorage.getItem("token-session-TIM") != null){
      return true;
    }
    return false
  }

  SetUserSession(userData: any) {
    localStorage.setItem("userData", JSON.stringify(userData))
  }
  getUserSession() {
    let sessionString = localStorage.getItem("userData")??"";

     return JSON.parse(sessionString);
  }
  setRole(role: string){
    sessionStorage.setItem("role", role);
  }

  getRole(){
    return sessionStorage.getItem("role");
  }
  setName(name: string){
    sessionStorage.setItem("name", name);
  }
  getName(){
    return sessionStorage.getItem("name");
  }
  setSapId(sapId: string){
    sessionStorage.setItem("sapId", sapId);
  }

  SetSession(key:string, value: string){
    localStorage.setItem(key, value);
  }
  GetSession(key:string):string | null{
    return localStorage.getItem(key);
  }

  ClearSession(){
    localStorage.clear();
    sessionStorage.clear();
  }

  ConvertToTimeStamp(datestring:string){
    return (new Date(datestring)).getTime() / 1000;
  }

  ExtractBase64String(data:string){
    if (data.startsWith("data:")) {
        let dataArray = data.split(',');
        return dataArray[1];
    }
    return data;
  }
  GetExtensionName(fileName:string){
    let nameArray = fileName.split('.');
    return nameArray[1];
  }
  GetFileName(fileName:string){
    let nameArray = fileName.split('.');
    return nameArray[0];
  }

  ValidateFiles(files:any){
    console.log(files);
    if (files.invalidFiles.length > 0) {
      return false;
    }
    return true;
  }

  MapTo<T>(data:any){
    let stringData = JSON.stringify(data);
    return JSON.parse(stringData) as T;
  }

  formatDate2(date: string){
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    let year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  ExportData(data:any[], filename:string, dataTitle: string){
    let dateSting  =  this.ConvertToTimeStamp((new Date()).toISOString());
          const options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalSeparator: '.',
            showLabels: true,
            showTitle: true,
            title: dataTitle,
            useTextFile: false,
            useBom: true,
            useKeysAsHeaders: true,
            filename: `Generated_${filename}_${dateSting}`
            // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
          };
          // const csvExporter = new ExportToCsv(options);
          // csvExporter.generateCsv(data);
  }

 ConvertToNumber(numWithString: string):number
 {
  let response = numWithString.split(',').join("");
  return  response as any as number;
 }

}
