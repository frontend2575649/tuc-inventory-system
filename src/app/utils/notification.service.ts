import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private _toastr: ToastrService) { }

  showSuccess(message: string = "Successful") {
    this._toastr.success(message, "Success", {closeButton: true})
  }

  
  showError(message:string ) {
    var errorMessage = message? message : "Dear customer, we tried processing your request. However, there seems to be a connectivity issue. We advise you try again shortly.";
    this._toastr.error(errorMessage, "Error", {closeButton: true})
  }

  showInfo(message: string) {
    this._toastr.info(message, "Info", {closeButton: true})
  }

  showWarning(message: string) {
    this._toastr.warning(message, "Warning", {closeButton: true});
  }

  clear(){
    this._toastr.clear()
  }

  networkError(){
    this.showError("Network Error, Please check your network and try again");
  }
  sessionError(){
    this.showError("Your session has expired. please login again to proceed");
  }

  forbiddenError(){
    this.showError("Sorry you are not permitted to perfom this action");
  }

  showMessage(issuccessful: boolean = true, message: string = "Successful") {
    if (issuccessful) {
      this._toastr.success(message, "Success", {closeButton: true})
    } else {
      this.showWarning(message);
    }
  }

  async Goback(){
    history.back();
    return true;
  }

}
